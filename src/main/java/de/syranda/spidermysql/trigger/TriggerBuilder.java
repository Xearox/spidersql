package de.syranda.spidermysql.trigger;

import de.syranda.spidermysql.ConnectionManager;

public class TriggerBuilder {

	private String name;
	private String table;
	
	private TriggerTime triggerTime;
	private TriggerEvent triggerEvent;
	
	private String forEachRow;
	
	public TriggerBuilder(String name, String table, TriggerTime triggerTime, TriggerEvent triggerEvent) {
		this.name = name;
		this.table = table;
		this.triggerTime = triggerTime;
		this.triggerEvent = triggerEvent;
		forEachRow = "";
	}
	
	public TriggerBuilder(String name) {
		this.name = name;
		forEachRow = "";		
	}
	
	public TriggerBuilder addStatement(String statement) {		
		forEachRow += statement + ";";
		return this;				
	}
	
	public TriggerBuilder setTriggerTime(TriggerTime triggerTime) {		
		this.triggerTime = triggerTime;
		return this;		
	}
	
	public TriggerBuilder setTriggerEvent(TriggerEvent triggerEvent) {		
		this.triggerEvent = triggerEvent;
		return this;		
	}
	
	public TriggerBuilder setTable(String table) {		
		this.table = table;
		return this;		
	}
	
	public void build() {				
		new Trigger(name).delete();		
		String query = "CREATE TRIGGER `" + name + "` " + triggerTime.getSyntax() + " " + triggerEvent.getSyntax() + " ON `" + table + "` FOR EACH ROW BEGIN " + forEachRow + " END;";
		ConnectionManager.insertStatement(query);		
	}
	
}
