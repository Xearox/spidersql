package de.syranda.spidermysql.trigger;

import de.syranda.spidermysql.ConnectionManager;

public class Trigger {

	private String name;
	
	public Trigger(String name) {
		this.name = name;		
	}
	
	public TriggerBuilder builder() {		
		return new TriggerBuilder(name);		
	}
	
	public TriggerBuilder builder(String table, TriggerTime triggerTime, TriggerEvent triggerEvent) {		
		return new TriggerBuilder(name, table, triggerTime, triggerEvent);		
	}
	
	public void delete() {		
		ConnectionManager.insertStatement("DROP TRIGGER IF EXISTS `" + name + "`");		
	}
	
}
