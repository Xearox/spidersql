package de.syranda.spidermysql.syntax;

public class MySQLDatabaseSyntax {

	private String databaseName;
	
	public MySQLDatabaseSyntax(String databaseName) {

		this.databaseName = databaseName;

	}
	
	public String create() {
		
		String query = "CREATE DATABASE IF NOT EXISTS `" + databaseName + "`";
		
		return query;
		
	}
	
	public String showTables() {
		
		String query = "SHOW TABLES IN `" + databaseName + "`";
		
		return query;
		
	}
	
	public String showTriggers() {
		
		String query = "SHOW TRIGGERS IN `" + databaseName + "`";
		
		return query;
		
	}
	
	public String showEvents() {
		
		String query = "SHOW EVENTS IN `" + databaseName + "`";
		
		return query;
		
	}
	
}
