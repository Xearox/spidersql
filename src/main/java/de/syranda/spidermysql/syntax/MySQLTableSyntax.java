package de.syranda.spidermysql.syntax;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.simple.JSONObject;

import de.syranda.spidermysql.ConnectionManager;
import de.syranda.spidermysql.ConventionFormatter;
import de.syranda.spidermysql.builder.ColumnType;
import de.syranda.spidermysql.builder.Exclude;
import de.syranda.spidermysql.builder.ExcludeField;
import de.syranda.spidermysql.builder.ForeignKey;
import de.syranda.spidermysql.builder.Key;
import de.syranda.spidermysql.builder.MySQLClass;
import de.syranda.spidermysql.serializer.ClassSerializer;
import de.syranda.spidermysql.table.MySQLField;
import de.syranda.spidermysql.table.MySQLTable;
import de.syranda.spidermysql.utils.Serializer;
import de.syranda.spidermysql.utils.Utils;

public class MySQLTableSyntax {

	private String tableName;
	private String database;

	public MySQLTableSyntax(String tableName) {
		this(tableName, null);
	}

	public MySQLTableSyntax(String tableName, String database) {
		this.tableName = tableName;
		this.database = database;
	}

	public MySQLTableSyntax(MySQLTable table) {
		this(table.getTableName(), table.getDatabase());
	}
	
	public String showColumn(String columnName) {
		return "SHOW COLUMNS FROM " + getTarget() + " LIKE '" + columnName + "'";
	}

	public String insert(String values) {
		values = Utils.replaceLastSemicolon(values);
		if (!checkSyntax(values))
			ConnectionManager.handleException(new IllegalArgumentException("Syntax error for query " + values), false);

		String query = "INSERT INTO " + getTarget() + " (" + getEscapedKeys(values, ", ") + ") VALUES ("
				+ getPreparedValues(values, ", ") + ")";

		return query;
	}

	public String insertUpdate(String values) {
		values = Utils.replaceLastSemicolon(values);
		if (!checkSyntax(values))
			ConnectionManager.handleException(new IllegalArgumentException("Syntax error for query " + values), false);

		String query = "INSERT INTO " + getTarget() + " (" + getEscapedKeys(values, ", ") + ") VALUES ("
				+ getPreparedValues(values, ", ") + ") ON DUPLICATE KEY UPDATE " + getEscapedKeyValueList(values, ", ");

		return query;
	}

	public String insertIgnore(String values) {
		values = Utils.replaceLastSemicolon(values);
		if (!checkSyntax(values))
			ConnectionManager.handleException(new IllegalArgumentException("Syntax error for query " + values), false);

		String query = "INSERT IGNORE INTO " + getTarget() + " (" + getEscapedKeys(values, ", ") + ") VALUES ("
				+ getPreparedValues(values, ", ") + ")";

		return query;
	}

	public String insert(Object clazz, String add) {
		return insert(getSpiderList(clazz, add));
	}

	public String insert(Object clazz) {
		return insert(clazz, "");
	}

	public String insertUpdate(Object clazz, String add) {
		return insertUpdate(getSpiderList(clazz, add));
	}

	public String insertUpdate(Object clazz) {
		return insertUpdate(clazz, "");
	}

	public String insertIgnore(Object clazz, String add) {
		return insertIgnore(getSpiderList(clazz, add));
	}

	public String insertIgnore(Object clazz) {
		return insertIgnore(clazz, "");
	}

	public String get(String what, String where) {
		if (!checkListSyntax(what) && !what.equalsIgnoreCase("*"))
			throw new IllegalArgumentException("Syntax error for query " + what);

		if (!checkSyntax(where))
			throw new IllegalArgumentException("Syntax error for query " + where);

		String query = "SELECT " + getEscapedKeys(what, ",") + " FROM " + getTarget() + " WHERE "
				+ getEscapedKeyValueList(where, " AND ");

		return query;
	}

	public String get(String where) {
		if (!checkSyntax(where))
			throw new IllegalArgumentException("Syntax error for query " + where);

		String query = "SELECT * FROM " + getTarget() + " WHERE " + getEscapedKeyValueList(where, "AND");

		return query;
	}

	public String update(String what, String where) {
		if (!checkSyntax(what))
			throw new IllegalArgumentException("Syntax error for query " + what);

		if (!checkSyntax(where) && !where.equals(""))
			throw new IllegalArgumentException("Syntax error for query " + where);

		String query = "UPDATE " + getTarget() + " SET " + getEscapedKeyValueList(what, ",");

		if (!where.equals(""))
			query += " WHERE " + getEscapedKeyValueList(where, "AND");

		return query;
	}

	public String update(String what) {
		if (!checkSyntax(what))
			throw new IllegalArgumentException("Syntax error for query " + what);

		return update(what, "");
	}

	public String delete(String where) {
		if (!checkSyntax(where))
			throw new IllegalArgumentException("Syntax error for query " + where);

		String query = "DELETE FROM " + getTarget() + " WHERE " + getEscapedKeyValueList(where, "AND");

		return query;
	}

	public String get() {
		return "SELECT * FROM " + getTarget() + "";
	}

	public String truncate() {
		String query = "TRUNCATE " + getTarget() + "";
		return query;
	}

	public String getTarget() {
		return (database != null ? "`" + database + "`." : "") + "`" + tableName + "`";
	}

	public String getDatabase() {
		return this.database;
	}

	public String changeColumn(String field, MySQLField newField) {
		String query = "ALTER TABLE " + getTarget() + " CHANGE COLUMN `" + field + "` `" + newField.getFieldName()
				+ "` " + newField.getType() + (newField.getLength() != 0 ? "(" + newField.getLength() + ")" : "");
		return query;
	}

	public String setColumnDefault(String field, Object newDefault) {
		String query = "ALTER TABLE " + getTarget() + " ALTER COLUMN `" + field + "`";

		if (newDefault == null)
			query += " DROP DEFAULT";
		else {

			int defaultValue;

			try {

				defaultValue = Integer.parseInt(newDefault.toString());
				query += " SET DEFAULT " + defaultValue;

			} catch (Exception e) {

				query += " SET DEFAULT '" + newDefault.toString() + "'";

			}

		}

		return query;
	}

	public String changeColumnType(MySQLField field) {
		String query = "ALTER TABLE " + getTarget() + " MODIFY COLUMN `" + field.getFieldName() + "` " + field.getType()
				+ (field.getLength() != 0 ? "(" + field.getLength() + ")" : "");

		if (field.isAutoIncrement())
			query += " AUTO_INCREMENT";

		return query;
	}

	public String addField(MySQLField field) {
		String query = "ALTER TABLE " + getTarget() + " ADD COLUMN `" + field.getFieldName() + "` " + field.getType()
				+ (field.getLength() != 0 ? "(" + field.getLength() + ")" : "");

		if (field.allowNull())
			query += " NOT NULL";
		if (field.getDefaultValue() != null) {

			int defaultValue;

			try {

				defaultValue = Integer.parseInt(field.getDefaultValue().toString());
				query += " DEFAULT " + defaultValue;

			} catch (Exception e) {

				query += " DEFAULT '" + field.getDefaultValue().toString() + "'";

			}

		}

		if (field.isAutoIncrement())
			query += " AUTO_INCREMENT";

		return query;
	}

	public String dropColumn(String field) {
		String query = "ALTER TABLE " + getTarget() + " DROP COLUMN `" + field + "`";
		return query;
	}

	public String dropPrimaryKey() {
		String query = "ALTER TABLE " + getTarget() + " DROP PRIMARY KEY";
		return query;
	}

	public String dropKey(String key) {
		String query = "ALTER TABLE " + getTarget() + " DROP KEY `" + key + "`";
		return query;
	}

	public String dropForeignKey(String foreignKey) {
		String query = "ALTER TABLE " + getTarget() + " DROP FOREIGN KEY `" + foreignKey + "`";
		return query;
	}

	public String addKey(Key key) {
		String query = "ALTER TABLE " + getTarget() + " ADD ";

		String keys = "";

		for (int i = 0; i < key.getColumns().length; i++)
			keys += i == 0 ? "`" + key.getColumns()[i] + "`" : ", `" + key.getColumns()[i] + "`";

		switch (key.getKeyType()) {
		case PRIMARY:
			query += "PRIMARY KEY (" + keys + ")";
			break;
		case UNIQUE:
			query += "UNIQUE KEY `" + key.getKeyName() + "` (" + keys + ")";
			break;
		default:
			break;
		}

		return query;
	}

	public String addForeignKey(ForeignKey key) {
		String query = "ALTER TABLE " + getTarget() + " ADD ";

		String targetC = "";

		for (int ii = 0; ii < key.getTargetColumns().length; ii++)
			targetC += ii == 0 ? "`" + key.getTargetColumns()[ii] + "`" : ", `" + key.getTargetColumns()[ii] + "`";

		String referenceC = "";

		for (int ii = 0; ii < key.getReferenceColumns().length; ii++)
			referenceC += ii == 0 ? "`" + key.getReferenceColumns()[ii] + "`"
					: ", `" + key.getReferenceColumns()[ii] + "`";

		query += "CONSTRAINT `" + key.getKeyName() + "` FOREIGN KEY (" + targetC + ") REFERENCES `"
				+ key.getRefrenceTable() + "` (" + referenceC + ") ON DELETE " + key.getOnDelete().getAction()
				+ " ON UPDATE " + key.getOnUpdate().getAction();

		return query;
	}

	public String drop() {
		String query = "DROP TABLE IF EXISTS " + getTarget();
		return query;
	}

	public String describe() {
		String query = "DESCRIBE " + getTarget();
		return query;
	}

	public String showKeys() {
		String query = "SELECT `information_schema`.`KEY_COLUMN_USAGE`.`CONSTRAINT_NAME`,"
				+ "`information_schema`.`KEY_COLUMN_USAGE`.`COLUMN_NAME`,"
				+ "`information_schema`.`KEY_COLUMN_USAGE`.`REFERENCED_TABLE_NAME`,"
				+ "`information_schema`.`KEY_COLUMN_USAGE`.`REFERENCED_COLUMN_NAME`,"
				+ "`information_schema`.`REFERENTIAL_CONSTRAINTS`.`UPDATE_RULE`,"
				+ "`information_schema`.`REFERENTIAL_CONSTRAINTS`.`DELETE_RULE` "
				+ "FROM `information_schema`.`KEY_COLUMN_USAGE`"
				+ "LEFT JOIN `information_schema`.`REFERENTIAL_CONSTRAINTS` "
				+ "ON `information_schema`.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_NAME` = `information_schema`.`KEY_COLUMN_USAGE`.`CONSTRAINT_NAME` "
				+ "WHERE `information_schema`.`KEY_COLUMN_USAGE`.`TABLE_NAME` = '" + tableName
				+ "' AND TABLE_SCHEMA = '" + database + "' "
				+ "GROUP BY `information_schema`.`KEY_COLUMN_USAGE`.`CONSTRAINT_NAME`, `information_schema`.`KEY_COLUMN_USAGE`.`COLUMN_NAME`";

		return query;
	}

	private static boolean checkListSyntax(String raw) {
		return raw.matches("^[^;]+(;[^;])*");
	}

	private static boolean checkSyntax(String raw) {
		return raw.matches("^[^;:]+:[^;]+(;[^;:]+:[^;]+)*");
	}

	public static String getEscapedKeys(String raw, String connector) {
		return Stream.of(raw.split(";")).map(entry -> "`" + entry.split(":", 2)[0].split(">")[0] + "`")
				.collect(Collectors.joining(connector));
	}

	@SuppressWarnings("unchecked")
	public static Object[] getEscapedValues(String raw) {
		HashMap<String, JSONObject> jsons = new HashMap<String, JSONObject>();
		StringBuilder builder = new StringBuilder(Stream.of(raw.split(";")).filter(entry -> {

			String key = entry.split(":", 2)[0];
			String value = entry.split(":", 2)[1];

			if (key.matches("[^>]+>[^>]+")) {
				String subkey = key.split(">")[0];
				JSONObject json = jsons.getOrDefault(subkey, new JSONObject());
				json.put(key.split(">", 2)[1], value);
				jsons.put(subkey, json);
				return false;
			}

			if (value.matches("^(OLD|NEW)(\\.{1})\\w([^\\.]*)"))
				return false;

			return true;

		}).map(entry -> entry.split(":", 2)[1]).collect(Collectors.joining(";")));

		if (!jsons.isEmpty())
			builder.append(
					" " + jsons.values().stream().map(json -> json.toJSONString()).collect(Collectors.joining(";")));

		return builder.toString().replace("true", "1").replace("false", "0").split(";");

	}

	@SuppressWarnings("unchecked")
	public static String getEscapedKeyValueList(String raw, String connector) {

		StringBuilder builder = new StringBuilder(
				Stream.of(raw.split(";")).filter(entry -> !entry.split(":", 2)[0].matches("[^>]+>[^>]+")).map(entry -> {

					String key = entry.split(":", 2)[0];
					String value = entry.split(":", 2)[1];

					return "`" + key + "`=" + (value.matches("^(OLD|NEW)(\\.{1})\\w([^\\.]*)")
							? value.split("\\.")[0] + ".`" + value.split("\\.")[1] + "`" : "?");

				}).collect(Collectors.joining(connector)));

		HashMap<String, JSONObject> jsons = new HashMap<String, JSONObject>();

		Stream.of(raw.split(";")).filter(entry -> entry.split(":", 2)[0].matches("[^>]+>[^>]+")).forEach(entry -> {

			String key = entry.split(":", 2)[0];
			String value = entry.split(":", 2)[1];

			String subkey = key.split(">")[0];

			JSONObject json = jsons.containsKey(subkey) ? jsons.get(subkey) : new JSONObject();
			json.put(key.split(">", 2)[1], value);

			jsons.put(subkey, json);

		});

		if (!jsons.isEmpty())
			builder.append(" "
					+ jsons.keySet().stream().map(field -> "`" + field + "`=?").collect(Collectors.joining(connector)));

		return builder.toString().replace("true", "1").replace("false", "0");

	}

	public static String getSpiderList(Object clazz, String add) {

		String list = "";

		boolean first = true;
		boolean adjust = clazz.getClass().isAnnotationPresent(MySQLClass.class)
				? clazz.getClass().getAnnotation(MySQLClass.class).adjustFields() : false;

		outer: for (int i = 0; i < clazz.getClass().getDeclaredFields().length; i++) {

			Field field = clazz.getClass().getDeclaredFields()[i];
			field.setAccessible(true);

			if ((!field.isAnnotationPresent(de.syranda.spidermysql.builder.MySQLField.class)
					&& !clazz.getClass().isAnnotationPresent(MySQLClass.class))
					|| field.isAnnotationPresent(Exclude.class) || Modifier.isStatic(field.getModifiers()))
				continue;

			String fieldName = adjust ? ConventionFormatter.getMySQLName(field.getName()) : field.getName();

			try {
				if (ClassSerializer.hasSerializer(field.getType())) {

					for (int index = 0; index < ClassSerializer.getSerializer(field.getType())
							.serializeObject(fieldName, field.get(clazz)).size(); index++) {

						@SuppressWarnings("unchecked")
						Entry<String, Object> entry = (Entry<String, Object>) ClassSerializer
								.getSerializer(field.getType()).serializeObject(fieldName, field.get(clazz)).entrySet()
								.toArray()[index];

						if (field.isAnnotationPresent(ExcludeField.class))
							for (String exField : field.getAnnotation(ExcludeField.class).fields())
								if (entry.getKey().equals(exField))
									continue outer;

						if (first) {
							list += entry.getKey() + ":" + entry.getValue();
							first = false;
						} else
							list += ";" + entry.getKey() + ":" + entry.getValue();

					}

				} else if (field.getType().isAnnotationPresent(MySQLClass.class)
						&& (field.isAnnotationPresent(de.syranda.spidermysql.builder.MySQLField.class) ? field
								.getDeclaredAnnotation(de.syranda.spidermysql.builder.MySQLField.class).serialize()
								: true)) {
					list += first ? getSpiderList(field.get(clazz), "") : ";" + getSpiderList(field.get(clazz), "");
				} else if (field.getType().isEnum() && ColumnType.getType(field.getType()) == ColumnType.NONE)
					list += first ? fieldName + ":" + ((Enum<?>) field.get(clazz)).name()
							: ";" + fieldName + ":" + ((Enum<?>) field.get(clazz)).name();
				else if (Serializable.class.isAssignableFrom(field.getType())
						&& ColumnType.getType(field.getType()) == ColumnType.NONE)
					list += first ? fieldName + ":" + Serializer.serialize(field.get(clazz))
							: ";" + fieldName + ":" + Serializer.serialize(field.get(clazz));
				else
					list += first ? fieldName + ":" + field.get(clazz).toString()
							: ";" + fieldName + ":" + field.get(clazz).toString();

			} catch (IllegalArgumentException e) {
			} catch (IllegalAccessException e) {
			}

			first = false;

		}

		if (!add.equals(""))
			list += ";" + add;

		return list;
	}

	public static String getPreparedValues(String raw, String connector) {
		return Stream.of(raw.split(";")).map(entry -> "?").collect(Collectors.joining(connector));
	}

	public String exists() {
		return "SELECT count(*) as count FROM `information_schema`.`tables` WHERE `table_name`=? AND `table_schema`=?";
	}

}
