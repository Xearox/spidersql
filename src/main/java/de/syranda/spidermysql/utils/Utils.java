package de.syranda.spidermysql.utils;

public class Utils {

	public static String replaceLastSemicolon(String str) {
	    if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ';') {
	        str = str.substring(0, str.length() - 1);
	    }
	    return str;
	}

}
