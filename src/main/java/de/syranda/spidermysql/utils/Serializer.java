package de.syranda.spidermysql.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.xml.bind.DatatypeConverter;

public class Serializer {
	
	public static String serialize(Object obj) {

		try {
		    ByteArrayOutputStream out = new ByteArrayOutputStream();
		    ObjectOutputStream os = new ObjectOutputStream(out);		
		    os.writeObject(obj);
		    return DatatypeConverter.printHexBinary(out.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
		
	}
	
	public static Object deserialize(String hex) {
		
		try {
		    ByteArrayInputStream in = new ByteArrayInputStream(DatatypeConverter.parseHexBinary(hex));
		    ObjectInputStream is = new ObjectInputStream(in);
		    return is.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;

	}

}
