package de.syranda.spidermysql.event;

/**
 * Class to handle an event's interval schedule
 * 
 * @author Boris
 *
 */
public class IntervalSchedule implements Schedule {
	
	private String scheduleString;
	
	/**
	 * Creates a new interval schedule.
	 * 
	 * @param quantity The amount of interval units
	 * @param interval The schedule's interval
	 */
	public IntervalSchedule(int quantity, Interval interval) {
		scheduleString = quantity + " " + interval.name();
	}

	/**
	 * Creates a new interval schedule.
	 * 
	 * @param quantity The amount of interval units
	 * @param interval The schedule's interval unit
	 */
	public IntervalSchedule(String quantity, Interval interval) {
		scheduleString = "'" + quantity + "' " + interval.name();
	}
	
	/**
	 * Sets when the interval schedule should start
	 * 
	 * @param schedule When the interval schedule should start
	 * @return {@link IntervalSchedule} Itself
	 */
	public IntervalSchedule starts(TimestampSchedule schedule) {		
		scheduleString += " STARTS " + schedule.getIntervalString();		
		return this;		
	}
	
	/**
	 * Sets when the interval schedule should end
	 * 
	 * @param schedule When the interval schedule should end
	 * @return {@link IntervalSchedule} Itself
	 */
	public IntervalSchedule ends(TimestampSchedule schedule) {		
		scheduleString += " ENDS " + schedule.getIntervalString();		
		return this;		
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getScheduleString() {
		return "EVERY " + scheduleString;		
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIntervalString() {
		return scheduleString;		
	}

}
