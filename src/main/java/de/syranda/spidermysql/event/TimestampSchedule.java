package de.syranda.spidermysql.event;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class to handle timestamp schedules
 * 
 * @author Boris
 *
 */
public class TimestampSchedule implements Schedule {

	private String scheduleString;
	
	/**
	 * Crates a new {@link TimestampSchedule} on the given timestamp
	 * 
	 * @param timestamp The schedules timestamp
	 */
	public TimestampSchedule(long timestamp) {
		scheduleString = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(timestamp)) + "'";		
	}
	
	/**
	 * Creates a new {@link TimestampSchedule} with the current timestamp
	 */
	public TimestampSchedule() {
		scheduleString = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + "'";		
	}
	
	/**
	 * Adds an interval to the schedule
	 * 
	 * @param quantity The amount of interval units
	 * @param interval The schedule's interval unit
	 * @return {@link TimestampSchedule} Iteself
	 */
	public TimestampSchedule addInterval(int quantity, Interval interval) {
		scheduleString += " + INTERVAL " + quantity + " " + interval.name();		
		return this;		
	}
	
	/**
	 * Adds an interval to the schedule
	 * 
	 * @param quantity The amount of interval units
	 * @param interval The schedule's interval unit
	 * @return {@link TimestampSchedule} Iteself
	 */
	public TimestampSchedule addInterval(String quantity, Interval interval) {
		scheduleString += " + INTERVAL '" + quantity + "' " + interval.name();		
		return this;		
	}
	
	/**
	 * {@inheritDoc}
	 */	
	@Override
	public String getScheduleString() {
		return "AT " + scheduleString;		
	}
	
	/**
	 * {@inheritDoc}
	 */	
	@Override
	public String getIntervalString() {
		return scheduleString;		
	}

}
