package de.syranda.spidermysql.event;

import de.syranda.spidermysql.ConnectionManager;

/**
 * Class to handle MySQL events
 * 
 * @author Boris
 *
 */
public class Event {

	private String name;
	
	/**
	 * Creates a new {@link Event} class
	 * 
	 * @param name The event's name
	 */
	public Event(String name) {
		this.name = name;
	}
	
	/**
	 * Get a new {@link EventBuilder}
	 * 
	 * @return {@link EventBuilder} A new {@link EventBuilder}
	 */
	public EventBuilder builder() {		
		return new EventBuilder(name);		
	}
	
	/**
	 * Deletes the event
	 */
	public void delete() {		
		ConnectionManager.insertStatement("DROP EVENT IF EXISTS `" + name + "`");		
	}
	
	/**
	 * Sets the event's status
	 * 
	 * @param enable Whether the event should be enabled
	 */
	public void setEnabled(boolean enable) {		
		ConnectionManager.insertStatement("ALTER EVENT `" + name + "` " + (enable ? "ENABLE" : "DISABLE"));		
	}

	/**
	 * Sets the event's schedule
	 * 
	 * @param schedule The new {@link Schedule}
	 */
	public void setSchedule(Schedule schedule) {
		ConnectionManager.insertStatement("ALTER EVENT `" + name + "` ON SCHEDULE " + schedule.getScheduleString());		
	}
	
}
