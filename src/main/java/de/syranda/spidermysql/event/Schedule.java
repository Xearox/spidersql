package de.syranda.spidermysql.event;

/**
 * Interface to handle event schedules
 * 
 * @author Boris
 *
 */
public interface Schedule {

	/**
	 * Gets the schedules schedule String
	 * = "EVERY|AT {@link Schedule#getIntervalString()}"
	 * 
	 * @return {@link String} The schedule's schedule String
	 */
	public String getScheduleString();	
	
	/**
	 * Gets the schedules interval String
	 * Basically the time transformet to text format
	 * 
	 * @return {@link String} The schedule's interval String
	 */
	public String getIntervalString();
	
}
