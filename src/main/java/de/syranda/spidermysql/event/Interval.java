package de.syranda.spidermysql.event;

/**
 * Enum to handle event intervals
 * 
 * @author Boris
 *
 */
public enum Interval {

    YEAR, QUARTER, MONTH, DAY, HOUR, MINUTE,
    WEEK, SECOND, YEAR_MONTH, DAY_HOUR, DAY_MINUTE,
    DAY_SECOND, HOUR_MINUTE, HOUR_SECOND, MINUTE_SECOND

}
