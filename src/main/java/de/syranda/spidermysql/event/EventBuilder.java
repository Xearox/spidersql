package de.syranda.spidermysql.event;

import de.syranda.spidermysql.ConnectionManager;
import de.syranda.spidermysql.syntax.MySQLTableSyntax;

/**
 * Class to build MySQL events
 * 
 * @author Boris
 *
 */
public class EventBuilder {

	private String name;
	private Schedule schedule;	
	private String task = "";
	private boolean preserve = true;
	private boolean enabled = true; 
	
	/**
	 * Creates a new {@link EventBuilder}
	 * 
	 * @param name The event's name
	 */
	public EventBuilder(String name) {		
		this.name = name;		
	}

	/**
	 * Sets the event's {@link Schedule}
	 * 
	 * @param schedule The event's {@link Schedule}
	 * @return {@link EventBuilder} Itself
	 */
	public EventBuilder setSchedule(Schedule schedule) {		
		this.schedule = schedule;		
		return this;		
	}
	
	/**
	 * Adds a MySQL query. This query is plain text.
	 * You can use the {@link MySQLTableSyntax} class for better statements.
	 * 
	 * @param statement The MySQL statement to add
	 * @return {@link EventBuilder} Itself
	 */
	public EventBuilder addStatement(String statement) {		
		task += statement + ";";		
		return this;		
	}
	
	/**
	 * Sets whether the event should be preserved after completion
	 * 
	 * @param preserve Whether the event should be preserved after completion
	 * @return {@link EventBuilder} Itself
	 */
	public EventBuilder setPreserve(boolean preserve) {		
		this.preserve = preserve;		
		return this;		
	}
	
	/**
	 * Sets whether the event should be enabled or not
	 * 
	 * @param enabled The event's status (true = enabled)
	 * @return {@link EventBuilder} Itself
	 */
	public EventBuilder setEnabled(boolean enabled) {		
		this.enabled = enabled;		
		return this;		
	}
	
	/**
	 * Builds the event
	 */
	public void build() {
		
		String query = "CREATE EVENT IF NOT EXISTS `" + name + "`"
				+ " ON SCHEDULE " + schedule.getScheduleString() 
				+ " ON COMPLETION" + (preserve ? "" : " NOT") 
				+ " PRESERVE " + (enabled ? "ENABLE" : "DISABLE")
				+ " DO BEGIN "
				+ task
				+ " END";
		
		ConnectionManager.insertStatement(query);
		
	}
	
}
