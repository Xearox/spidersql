package de.syranda.spidermysql.helper;

import de.syranda.spidermysql.builder.ColumnType;
import de.syranda.spidermysql.builder.ForeignKey;
import de.syranda.spidermysql.builder.ForeignKeyAction;
import de.syranda.spidermysql.builder.Key;
import de.syranda.spidermysql.builder.KeyType;
import de.syranda.spidermysql.builder.TableBuilder;
import de.syranda.spidermysql.table.MySQLField;
import de.syranda.spidermysql.table.MySQLTable;

public class IdTable extends MySQLTable {

	private MySQLField referenceField;
	
	public IdTable(String tableName, String database) {
		super(tableName, database);
	}

	public IdTable(String tableName) {
		super(tableName);
	}
	
	@Override
	public TableBuilder builder() {
		return super.builder().addField(new IdField());
	}
	
	public TableBuilder builder(MySQLField reference) {
		this.referenceField = reference;
		return super.builder().addField(new IdField()).addField(reference).addKey(new Key(reference.getFieldName() + "_key", KeyType.UNIQUE, reference.getFieldName()));
	}
	
	public TableBuilder referenceBuilder(String tableName) {		
		return referenceField != null ? new TableBuilder(tableName).addField(new IdField()).addField(referenceField)
				.addForeignKey(new ForeignKey("FK_" + getTableName() + "_" + tableName, getTableName(), ForeignKeyAction.CASCADE, ForeignKeyAction.CASCADE)
						.setReferenceColumns(referenceField.getFieldName())
						.setTargetColumns(referenceField.getFieldName())) 
				: new TableBuilder(tableName).addField(new MySQLField("id", ColumnType.INT))
				.addForeignKey(new ForeignKey("FK_" + getTableName() + "_" + tableName, getTableName(), ForeignKeyAction.CASCADE, ForeignKeyAction.CASCADE)
						.setReferenceColumns("id")
						.setTargetColumns("id"));
	}
	
}
