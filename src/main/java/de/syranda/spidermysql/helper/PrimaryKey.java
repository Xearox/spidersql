package de.syranda.spidermysql.helper;

import de.syranda.spidermysql.builder.Key;
import de.syranda.spidermysql.builder.KeyType;

/**
 * A {@link Key} with the name "" and {@link KeyType} PRIMARY<br/>
 * <br/>
 * 
 * = {@code new Key("", KeyType.PRIMARY, String... columns)}
 * 
 * @author Boris
 *
 */
public class PrimaryKey extends Key{

	public PrimaryKey(String... columns) {
		super("", KeyType.PRIMARY, columns);
	}
	
}
