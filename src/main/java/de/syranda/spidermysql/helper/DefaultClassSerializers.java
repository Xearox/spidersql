package de.syranda.spidermysql.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import de.syranda.spidermysql.serializer.ClassSerializer;
import de.syranda.spidermysql.serializer.SerializerInformation;

/**
 * Registeres some practial default {@link ClassSerializer}
 * 
 * @author Boris
 *
 */
public class DefaultClassSerializers {

	static {
		loadDefaultClassSerializers();
	}
	
	/**
	 * Registeres some practial default {@link ClassSerializer}
	 */
	@SuppressWarnings("rawtypes")
	public static void loadDefaultClassSerializers() {
		
		ClassSerializer.addClassSerializer(JSONObject.class, new ClassSerializer<JSONObject>() {
			
			@Override
			public SerializerInformation<JSONObject> serialize(String fieldName) {
				SerializerInformation<JSONObject> ret = new SerializerInformation<>();				
				ret.addString(fieldName, json -> json.toJSONString());				
				return ret;				
			}
			
			@Override
			public JSONObject deserialize(String fieldName, HashMap<String, Object> columns) {
				
				try {
					return (JSONObject) new JSONParser().parse(columns.get(fieldName).toString());
				} catch (ParseException e) {
					return null;
				}
				
			}
		});
		
		ClassSerializer.addClassSerializer(List.class, new ClassSerializer<List>() {

			@SuppressWarnings({ "unchecked" })
			@Override
			public SerializerInformation<List> serialize(String fieldName) {

				SerializerInformation<List> ret = new SerializerInformation<>();
				ret.addString(fieldName, list -> {
					JSONArray array = new JSONArray();
					array.addAll(list);
					return array.toJSONString();
				});
				
				return ret;
				
			}

			@SuppressWarnings({ "unchecked" })
			@Override
			public List deserialize(String fieldName, HashMap<String, Object> columns) {

				List ret = new ArrayList<>();
				
				JSONArray array = null;
				
				try {
					array = (JSONArray) new JSONParser().parse((String) columns.get(fieldName));
				} catch (ParseException e) {
					return null;
				}
				
				array.forEach(entry -> ret.add(entry));
				
				return ret;
				
			}
		});
		
	}
	
}
