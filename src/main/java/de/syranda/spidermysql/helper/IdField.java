package de.syranda.spidermysql.helper;

import de.syranda.spidermysql.builder.ColumnType;
import de.syranda.spidermysql.table.MySQLField;

/**
 * A {@link MySQLField} with the field name "id" and an auto incremention and primary key flag<br/>
 * <br/>
 * = {@code new MySQLField("id", ColumnType.INT).setAutoIncrement(true).setPrimaryKey(true);}
 * 
 * @author Boris
 *
 */
public class IdField extends MySQLField {

	public IdField() {
		super("id", ColumnType.INT);
		
		setAutoIncrement(true);
		setPrimaryKey(true);
		
	}
	
}
