package de.syranda.spidermysql;

/**
 * ExceptionHandler for exceptions thrown by the API
 * 
 * @author Boris
 *
 */
public interface SpiderExceptionHandler {
	
	/**
	 * Handles the thrown exception
	 * 
	 * @param exception The exception which has been thrown
	 * @param queryExecuted Whether the query has been executed
	 */
	public void handleException(Exception exception, boolean queryExecuted);

}
