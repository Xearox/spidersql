package de.syranda.spidermysql;

import java.util.ArrayList;
import java.util.List;

import de.syranda.spidermysql.table.MySQLField;

/**
 * 
 * Singleton
 * Manages the convetion formatting for {@link MySQLField}s
 * 
 * @author Boris
 *
 */
public final class ConventionFormatter {

	// Singleton
	private ConventionFormatter() {
	}
	
	/**
	 * Formats a String
	 * Only used internally
	 * 
	 * @param name The String you want to format
	 * @return {@link String} The formatted String
	 */
	public static String getMySQLName(String name) {		
		List<String> letters = new ArrayList<String>();
		
		for(int i = 0; i < name.length(); i++)
			if(Character.isUpperCase(name.charAt(i)) && i != 0 && !Character.isUpperCase(name.charAt(i - 1)))
				letters.add("_" + Character.toLowerCase(name.charAt(i)));
			else
				letters.add(Character.toLowerCase(name.charAt(i)) + "");
			
		StringBuilder ret = new StringBuilder();
		
		for(String letter : letters)
			ret.append(letter);
			
		return ret.toString();		
	}
	
}
