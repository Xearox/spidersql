package de.syranda.spidermysql.builder;

/**
 * Enum for foreign key actions
 * 
 * @author Boris
 *
 */
public enum ForeignKeyAction {

	NO_ACTION("NO ACTION"), CASCADE("CASCADE"), RESTRICT("RESTRICT"), SET_NULL("SET NULL");
	
	private String action;
	
	private ForeignKeyAction(String action) {	
		this.action = action;		
	}
	
	/**
	 * Gets the action
	 * 
	 * @return {@link String} The action
	 */
	public String getAction() {		
		return this.action;		
	}
	
}
