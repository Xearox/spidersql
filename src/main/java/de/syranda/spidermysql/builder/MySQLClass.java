package de.syranda.spidermysql.builder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Anotation to mark classes for the {@link TableBuilder}
 * 
 * @author Boris
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface MySQLClass {

	/**
	 * Gets whether the field names should be converted
	 * 
	 * @return Whether the field names should be converted
	 */
	boolean adjustFields() default false;
	
}
