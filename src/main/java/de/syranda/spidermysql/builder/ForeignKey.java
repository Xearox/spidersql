package de.syranda.spidermysql.builder;

/**
 * Holds information about a foreign key
 * 
 * @author Boris
 *
 */
public class ForeignKey {

	private final String keyName;
	private String[] columns;
	private final String reference;
	private String[] referenceColumns;
		
	private final ForeignKeyAction onUpdate;
	private final ForeignKeyAction onDelete;
	
	/**
	 * Creates a new foreign key
	 * 
	 * @param keyName The name of the foreign key
	 * @param reference Which table the foreign key references to
	 * @param onUpdate The action when an entry is updated
	 * @param onDelete The action when an entry is deleted
	 */
	public ForeignKey(String keyName, String reference, ForeignKeyAction onUpdate, ForeignKeyAction onDelete) {
		this.keyName = keyName;
		this.reference = reference;
		
		this.onUpdate = onUpdate;
		this.onDelete = onDelete;	
	}	
	
	/**
	 * Sets the target columns of the foreign key
	 * 
	 * @param columns Which columns the {@link ForeignKey} should target
	 * @return {@link ForeignKey} Itself
	 */
	public ForeignKey setTargetColumns(String... columns) {		
		this.columns = columns;		
		return this;		
	}
	
	/**
	 * Sets the columns this foreign key references to
	 * 
	 * @param columns Which columns the {@link ForeignKey} references to
	 * @return {@link ForeignKey} Itself
	 */
	public ForeignKey setReferenceColumns(String... columns) {		
		this.referenceColumns = columns;	
		return this;		
	}
	
	/**
	 * Gets the foreign key's name
	 * 
	 * @return {@link String} The foregin key's name
	 */
	public String getKeyName() {		
		return this.keyName;		
	}

	/**
	 * Get the table the foreign key references to
	 * 
	 * @return {@link String} The table the foreign key references to
	 */
	public String getRefrenceTable() {		
		return this.reference;
	}
	
	/**
	 * Gets the column names targeted by this foreign key
	 * 
	 * @return String[] The column names targeted by this foreign key
	 */
	public String[] getTargetColumns() {		
		return this.columns;		
	}
	
	/**
	 * Gets the columns this foreign key references to
	 * 
	 * @return String[] The columns this foreign key references to
	 */
	public String[] getReferenceColumns() {		
		return this.referenceColumns;		
	}
	
	/**
	 * Gets the action when an entry is updated
	 * 
	 * @return {@link ForeignKeyAction} The action when an entry is updated
	 */
	public ForeignKeyAction getOnUpdate() {		
		return this.onUpdate;		
	}
	
	/**
	 * Gets the action when an entry is deleted
	 * 
	 * @return {@link ForeignKeyAction} The action when an entry is deleted
	 */
	public ForeignKeyAction getOnDelete() {		
		return this.onDelete;		
	}
	
}
