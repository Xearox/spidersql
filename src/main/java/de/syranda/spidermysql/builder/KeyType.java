package de.syranda.spidermysql.builder;

/**
 * Enum for key types
 * 
 * @author Boris
 *
 */
public enum KeyType {
	PRIMARY, UNIQUE;	
}

