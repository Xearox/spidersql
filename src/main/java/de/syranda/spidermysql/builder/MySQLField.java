package de.syranda.spidermysql.builder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to override the default values of fields which parent class has been marked
 * by the {@link MySQLClass} annotation
 * 
 * @author Boris
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MySQLField {

	/**
	 * Gets the MySQL column type of the field
	 * 
	 * @return {@link ColumnType} The MySQL column type of the field
	 */
	ColumnType type() default ColumnType.NONE;
	
	/**
	 * Gets the default length of the field
	 * 
	 * @return {@link Integer} The default length of the field
	 */	
	int length() default 0;
	
	/**
	 * Gets whether the field can be null
	 * 
	 * @return {@link Boolean} Whether the field can be null
	 */
	boolean notNull() default false;
	
	/**
	 * Gets whether the field has an auto incremention flag
	 * 
	 * @return {@link Boolean} Whether the field has an auto incremention flag
	 */
	boolean autoIncrement() default false;
	
	/**
	 * Gets whether the field has an primary key flag
	 * 
	 * @return {@link Boolean} Whether the field has an primary key flag
	 */
	boolean isPrimaryKey() default false;
	
	/**
	 * Gets whether the field should be serialized
	 * 
	 * @return {@link Boolean} Whetnher the field should be serialized
	 */
	boolean serialize() default true;
	
}
