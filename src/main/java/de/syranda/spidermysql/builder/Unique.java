package de.syranda.spidermysql.builder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark fields which should be treated as unique keys
 * 
 * @author Boris
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Unique {

	/**
	 * Gets the unique key's name
	 * 
	 * @return {@link String} The unique key's name
	 */
	String name();
	
}
