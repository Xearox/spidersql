package de.syranda.spidermysql.builder;

import java.util.Arrays;

/**
 * Holds information about a key
 * 
 * @author Boris
 *
 */
public class Key {

	private final String name;
	private final KeyType keyType;
	private String[] coloumns;
	
	/**
	 * Creates a new key
	 * 
	 * @param name The key name
	 * @param keyType The {@link KeyType}
	 * @param coloumns The columns the key references to
	 */
	public Key(String name, KeyType keyType, String... coloumns) {		
		this.name = name;
		this.keyType = keyType;
		this.coloumns = coloumns;		
	}
	
	/**
	 * Creates a new key
	 * 
	 * @param name The key name
	 * @param keyType The {@link KeyType}
	 */
	public Key(String name, KeyType keyType) {		
		this.name = name;
		this.keyType = keyType;
		this.coloumns = new String[0];
	}
	
	/**
	 * Gets the key name
	 * 
	 * @return {@link String} The Key name
	 */
	public final String getKeyName() {		
		return this.name;		
	}
	
	/**
	 * Gets the key type
	 * 
	 * @return {@link KeyType} The Key type
	 */
	public final KeyType getKeyType() {		
		return keyType;		
	}
	
	/**
	 * Gets the columns this key references to
	 * 
	 * @return String[] The columns this key references to
	 */
	public final String[] getColumns() {		
		return this.coloumns;		
	}
	
	/**
	 * Append a column to a key
	 * 
	 * @param append The column you want to append
	 */
	public final void appendColmun(String append) {
		String[] temp = Arrays.copyOf(this.coloumns, this.coloumns.length + 1);
		temp[temp.length - 1] = append;
		this.coloumns = temp;
	}
	
}
