package de.syranda.spidermysql.builder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.syranda.spidermysql.serializer.ClassSerializer;

/**
 * Annotation to mark fields which should be ignored by {@link ClassSerializer}s
 * 
 * @author Boris
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcludeField {
	String[] fields();
}