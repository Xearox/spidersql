package de.syranda.spidermysql.builder;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Stream;

import de.syranda.spidermysql.ConnectionManager;
import de.syranda.spidermysql.ConventionFormatter;
import de.syranda.spidermysql.serializer.ClassSerializer;
import de.syranda.spidermysql.table.MySQLField;
import de.syranda.spidermysql.utils.ConfigValues;

/**
 * Class to build MySQL tables
 * 
 * @author Boris
 *
 */
public class TableBuilder implements Cloneable {

	private final String tableName;	
	private final String database;
	
	private List<MySQLField> fields = new ArrayList<MySQLField>();
	private List<Key> keys = new ArrayList<Key>();
	private List<ForeignKey> fKeys = new ArrayList<ForeignKey>();
	
	/**
	 * Creates a new {@link TableBuilder} on the default database
	 * 
	 * @param tableName The table's name
	 */
	public TableBuilder(String tableName) {
		this(tableName, ConfigValues.MYSQL_DATABASE);
	}
	
	/**
	 * Creates a new {@link TableBuilder}
	 * 
	 * @param tableName The table's name
	 * @param database The databse the table should be build on
	 */
	public TableBuilder(String tableName, String database) {
		this.tableName = tableName;
		this.database = database;		
	}
	
	/**
	 * Gets the table's name
	 * 
	 * @return {@link String} The table's name
	 */
	public String getTableName() {		
		return this.tableName;		
	}
	
	/**
	 * Adds an {@link MySQLField}
	 * 
	 * @param field The {@link MySQLField} to add
	 * @return {@link TableBuilder} Itself
	 */
	public TableBuilder addField(MySQLField field) {		
		fields.add(field);		
		return this;		
	}
	
	public TableBuilder addField(String fieldName, ColumnType type) {
		fields.add(new MySQLField(fieldName, type));
		return this;
	}
	
	/**
	 * Adds the {@link MySQLField}s based on the Java field type's {@link ClassSerializer}
	 * <b>Note:</b> Make sure the given Java field type has a registred {@link ClassSerializer}
	 * 
	 * @param fieldName The field's name
	 * @param field The Java field type
	 * @param exclude A list of field names which should be excluded
	 * @return {@link TableBuilder} Iteself
	 */
	public TableBuilder addField(String fieldName, Class<?> field, String[] exclude, List<String> onKey) {
		
		ClassSerializer<?> serializer = ClassSerializer.getSerializer(field);
		
		for (Entry<String, Class<?>> entry : serializer.getPattern(fieldName).entrySet()) {
						
			if (exclude != null && Stream.of(exclude).anyMatch(string -> string.equals(entry.getKey()))){
				continue;
			}
			
			if (onKey != null) {
				onKey.add(entry.getKey());
			}
			
			if (ClassSerializer.hasSerializer(entry.getValue())) {
				this.addField(fieldName, entry.getValue(), exclude, onKey);
			} else {
				this.addField(new MySQLField(entry.getKey(), ColumnType.getType(entry.getValue())));		
			}
			
		}
		
		return this;
		
	}
	
	/**
	 * Adds a foreign key
	 * 
	 * @param key The foreign key to add
	 * @return {@link TableBuilder} Itself
	 */
	public TableBuilder addForeignKey(ForeignKey key) {		
		fKeys.add(key);
		return this;		
	}
	
	/**
	 * Adds a key
	 * 
	 * @param key The key to add
	 * @return {@link TableBuilder} Itself
	 */
	public TableBuilder addKey(Key key) {		
		keys.add(key);		
		return this;		
	}
	
	/**
	 * If the given class is marked with the {@link MySQLClass} annotation all fields (except those which are marked with the {@link Exclude} annotation)
	 * will be added
	 * 
	 * If the given class is <b>not</b> marked with the {@link MySQLClass} annotation all fields marked with the {@link MySQLField} annotation will be added
	 * 
	 * @param clazz The class
	 * @return {@link TableBuilder} Itself
	 */
	public TableBuilder addFieldsOfClass(Class<?> clazz) {
		
		HashMap<String, List<String>> keys = new HashMap<String, List<String>>();
		
		boolean adjust = clazz.isAnnotationPresent(MySQLClass.class) ? clazz.getAnnotation(MySQLClass.class).adjustFields() : false;
		
		for(Field field : clazz.getDeclaredFields()) {
			
			if((!field.isAnnotationPresent(de.syranda.spidermysql.builder.MySQLField.class) && !clazz.isAnnotationPresent(MySQLClass.class)) || field.isAnnotationPresent(Exclude.class) || Modifier.isStatic(field.getModifiers()))
				continue;
			
			de.syranda.spidermysql.builder.MySQLField ann = field.getAnnotation(de.syranda.spidermysql.builder.MySQLField.class);			
			
			if(ClassSerializer.hasSerializer(field.getType())) {
						
				String keyName = null;
				
				if (field.isAnnotationPresent(Unique.class)) {
					keyName = field.getDeclaredAnnotation(Unique.class).name();
				} else if (field.isAnnotationPresent(Primary.class)) {
					keyName = "primary";					
				} 
				
				List<String> onKey = keyName != null ? keys.getOrDefault(keyName, new ArrayList<>()) : null;
				addField(adjust ? ConventionFormatter.getMySQLName(field.getName()) : field.getName(), field.getType(), field.isAnnotationPresent(ExcludeField.class) ? field.getAnnotation(ExcludeField.class).fields() : null, onKey);
				
				if (onKey != null) {
					keys.put(keyName, onKey);
				}
				
				continue;
				
			} else if(field.getType().isAnnotationPresent(MySQLClass.class) && ann != null && ann.serialize()) {
				
				addFieldsOfClass(field.getType());
				continue;
				
			}
			
			ColumnType type = ColumnType.getType(field.getType());		
			
			MySQLField mField = new MySQLField(adjust ? ConventionFormatter.getMySQLName(field.getName()) : field.getName(), type);

			mField.setLength(type.getDefaultLength());
			
			if(ann != null) {
			
				if(ann.type() != ColumnType.NONE)
					mField.setType(ann.type());
				if(ann.length() != 0)
					mField.setLength(ann.length());
				mField.setAllowNull(!ann.notNull());
				mField.setAutoIncrement(ann.autoIncrement());
				mField.setPrimaryKey(ann.isPrimaryKey());
				
			}
			
			if(mField.getType().equalsIgnoreCase("none"))
				continue;
			
			if(field.isAnnotationPresent(Primary.class))
				if(!keys.containsKey("primary")) {
					ArrayList<String> list = new ArrayList<>();
					list.add(field.getName());
					keys.put("primary", list);
				} else {
					
					List<String> list = keys.get("primary");
					list.add(field.getName());
					keys.put("primary", list);
					
				}
			
			if(field.isAnnotationPresent(Unique.class))	{				
				List<String> list = !keys.containsKey(field.getAnnotation(Unique.class).name()) ? new ArrayList<String>() : keys.get(field.getAnnotation(Unique.class).name());
				list.add(field.getName());
				keys.put(field.getAnnotation(Unique.class).name(), list);		
			}
			
			addField(mField);
			
		}
		
		for(String key : keys.keySet()) {		
			
			String[] fields = new String[keys.get(key).size()];
			for(int i = 0; i < fields.length; i++)
				fields[i] = keys.get(key).get(i);
			
			if(key.equals("primary")) 
				addKey(new Key("", KeyType.PRIMARY, fields));
			else
				addKey(new Key(key, KeyType.UNIQUE, fields));		
		
		}
			
		return this;
		
	}
	

	/**
	 * Same as {@link TableBuilder#build(true)}
	 */
	public void build() {
		build(true);
	}
	
	/**
	 * Builds the table
	 * 
	 * @param relative Whether the table is build relativly to the database the query is executed on
	 */
	public void build(boolean relative) {
		
		String query = "CREATE TABLE IF NOT EXISTS " + (relative ? "`" + getTableName() + "`" : getTarget()) + " (";
		
		for(int i = 0; i < fields.size(); i++) {
			
			MySQLField field = fields.get(i);
			
			if(i != 0)
				query += ", ";
			
			query += "`" + field.getFieldName() + "` " + field.getType() + (field.getLength() != 0 ? "(" + field.getLength() + ")" : "");
			
			if(!field.allowNull())
				query += " NOT NULL";
			
			if(field.getDefaultValue() != null) { 
				
				int defaultValue;
				
				try {
				
					defaultValue = Integer.parseInt(field.getDefaultValue().toString());
					query += " DEFAULT " + defaultValue;
					
				} catch (Exception e) {
					
					query += " DEFAULT '" + field.getDefaultValue().toString() + "'";
					
				}
				
			}
			
			if(field.isAutoIncrement())
				query += " AUTO_INCREMENT";
			
			if(field.isPrimaryKey())
				query += " PRIMARY KEY";
			
		}
		
		if(!keys.isEmpty()) {				
			
			for(Key key : keys) {
				
				if (key == null || key.getColumns() == null) {
					continue;
				}
				
				String keys = "";
				
				for(int i = 0; i < key.getColumns().length; i++)
					keys += i == 0 ? "`" + key.getColumns()[i] + "`" : ", `" + key.getColumns()[i] + "`";
				
				switch (key.getKeyType()) {
				case PRIMARY:
					query += ", PRIMARY KEY (" + keys + ")";
					break;
				case UNIQUE:
					query += ", UNIQUE KEY `" + key.getKeyName() + "` (" + keys + ")";
					break;
				default:
					break;
				}
				
			}
			
		}
		
		if(!fKeys.isEmpty()) {
			
			query += ", ";
			
			for(int i = 0; i < fKeys.size(); i++) {
				
				ForeignKey key = fKeys.get(i);
				
				if(i != 0)
					query += ", ";
				
				String targetC = "";
				
				for(int ii = 0; ii < key.getTargetColumns().length; ii++)
					targetC += ii == 0 ? "`" + key.getTargetColumns()[ii] + "`" : ", `" + key.getTargetColumns()[ii] + "`";
				
				String referenceC = "";				
				
				for(int ii = 0; ii < key.getReferenceColumns().length; ii++)
					referenceC += ii == 0 ? "`" + key.getReferenceColumns()[ii] + "`" : ", `" + key.getReferenceColumns()[ii] + "`";
				
				query += "CONSTRAINT `" + key.getKeyName() + "` FOREIGN KEY (" + targetC + ") REFERENCES `" + key.getRefrenceTable() + "` (" + referenceC + ") ON DELETE " + key.getOnDelete().getAction() + " ON UPDATE " + key.getOnUpdate().getAction();
				
			}
			
		}
		
		query += ") ENGINE=InnoDB";	
		
		ConnectionManager.insertStatement(query);
		
	}

	/**
	 * Gets the table builder's target
	 * 
	 * @return {@link String} The table builder's target
	 */
	public String getTarget() {
		return "`" + database + "`.`" + tableName + "`";
	}
	
	/**
	 * Get on which database the table should be build on
	 * 
	 * @return {@link String} The table builder's database
	 */
	public String getDatabase() {		
		return this.database;		
	}

	/**
	 * Gets all registered MySQL fields
	 * 
	 * @return List {@link MySQLField} The registered MySQL fields
	 */
	public List<MySQLField> getFields() {
		return this.fields;		
	}
	
	/**
	 * Gets all registered Keys
	 * 
	 * @return List {@link Key} The registered keys
	 */
	public List<Key> getKeys() {		
		return this.keys;		
	}
	
	/**
	 * Gets all registered foreign keys
	 * 
	 * @return List {@link ForeignKey} The registered foreign keys
	 */
	public List<ForeignKey> getForeignKeys() {		
		return this.fKeys;		
	}
	
}
