package de.syranda.spidermysql.builder;

import java.io.Serializable;

/**
 * Enum for MySQL column types
 * 
 * @author Boris
 *
 */
public enum ColumnType {

	INT("int", 11), LONG("bigint", 20), SHORT("smallint", 6), DOUBLE("double", 0), STRING("varchar", 50), BOOLEAN("tinyint", 4), NONE("NONE", 0), TEXT("text", 0);
	
	private String stringType;
	private int defaultLength;
	
	private ColumnType(String stringType, int defaultLength) {
		this.stringType = stringType;
		this.defaultLength = defaultLength;
	}
	
	/**
	 * Gets the MySQL type name
	 * 
	 * @return {@link String} 
	 */
	public String getTypeName() {		
		return this.stringType;		
	}
	
	/**
	 * Gets the default length of the MySQL type
	 * 
	 * @return {@link Integer}
	 */
	public int getDefaultLength() {		
		return this.defaultLength;		
	}

	/**
	 * Gets the {@link ColumnType} of the given Java field type
	 * 
	 * @param field The Java field type
	 * @return {@link ColumnType} The matching MySQL type
	 */
	public static ColumnType getType(Class<?> field) {
		
		switch(field.getSimpleName().toLowerCase()) {
			case "int":
				return ColumnType.INT;
			case "integer":
				return ColumnType.INT;
			case "long":
				return ColumnType.LONG;
			case "short":
				return ColumnType.SHORT;
			case "float":
				return ColumnType.DOUBLE;
			case "double":
				return ColumnType.DOUBLE;
			case "string":
				return ColumnType.STRING;
			case "boolean":
				return ColumnType.BOOLEAN;
			default:
				if(Serializable.class.isAssignableFrom(field))
					return ColumnType.TEXT;
				else
					return null;
		}
		
	}
	
}
