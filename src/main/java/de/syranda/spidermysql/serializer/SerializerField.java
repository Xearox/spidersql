package de.syranda.spidermysql.serializer;

import java.util.function.Function;

public class SerializerField<N> {

	private String fieldName;
	private Function<? super N, ?> function;
	private Class<?> type;
	
	public SerializerField(String fieldName, Class<?> type, Function<? super N, ?> function) {

		this.fieldName = fieldName;
		this.function = function;
		this.type = type;
		
	}
	
	public String getFieldName() {
		return fieldName;
	}
	
	public Function<? super N, ?> getFunction() {
		return function;
	}
	
	public Class<?> getType() {
		return type;
	}
	
}
