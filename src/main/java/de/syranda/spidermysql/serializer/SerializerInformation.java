package de.syranda.spidermysql.serializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
@SuppressWarnings("rawtypes")
public class SerializerInformation <T> {

	private List<SerializerField> fields = new ArrayList<SerializerField>();	

	public List<SerializerField> getFields() {
		return fields;
	}
	
	@SuppressWarnings("unchecked")
	public <N> void add(String fieldName, Class<?> type, Function<? extends T, N> function) {
		fields.add(new SerializerField(fieldName, type, function));
	}
	
	public <N> void addString(String fieldName, Function<? extends T, N> function) {
		add(fieldName, String.class, function);
	}
	
	public <N> void addBoolean(String fieldName, Function<? extends T, N> function) {
		add(fieldName, Boolean.class, function);
	}
	
	public <N> void addLong(String fieldName, Function<? extends T, N> function) {
		add(fieldName, Long.class, function);
	}
	
	public <N> void addInt(String fieldName, Function<? extends T, N> function) {
		add(fieldName, Integer.class, function);
	}
	
	public <N> void addShort(String fieldName, Function<? extends T, N> function) {
		add(fieldName, Short.class, function);
	}
	
	public <N> void addFloat(String fieldName, Function<? extends T, N> function) {
		add(fieldName, Float.class, function);
	}
	
	public <N> void addDouble(String fieldName, Function<? extends T, N> function) {
		add(fieldName, Double.class, function);
	}
	
	public <N> void addText(String fieldName, Function<? extends T, N> function) {
		add(fieldName, Serializable.class, function);
	}
	
	public HashMap<String, Class<?>> getPattern() {
		HashMap<String, Class<?>> ret = new HashMap<>();		
		getFields().forEach(entry -> ret.put(entry.getFieldName(), entry.getType()));
		return ret;				
	}
	
	@SuppressWarnings("unchecked")
	public <N> HashMap<String, Object> serialize(Object obj) {		
		HashMap<String, Object> ret = new HashMap<>();
		getFields().forEach(entry -> ret.put(entry.getFieldName(), entry.getFunction().apply((N) obj)));
		return ret;		
	}
	
}
