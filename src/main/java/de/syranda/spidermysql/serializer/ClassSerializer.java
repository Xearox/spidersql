package de.syranda.spidermysql.serializer;

import java.util.HashMap;

/**
 * Manages all {@link ClassSerializer}s
 * 
 * @author Boris
 *
 * @param <N> The class the serializer handles
 */
public abstract class ClassSerializer<N> {

	private static HashMap<Class<?>, ClassSerializer<?>> serializers = new HashMap<Class<?>, ClassSerializer<?>>();
	
	public static void addClassSerializer(Class<?> clazz, ClassSerializer<?> serializer) {		
		serializers.put(clazz, serializer);		
	}
	
	public static boolean hasSerializer(Class<?> clazz) {		
		return serializers.containsKey(clazz);		
	}
	
	public static ClassSerializer<?> getSerializer(Class<?> clazz) {		
		return serializers.get(clazz);		
	}
	
	public final HashMap<String, Object> serializeObject(String fieldName, Object object) {		
		return serialize(fieldName).serialize(object);		
	}
	
	public final HashMap<String, Class<?>> getPattern(String fieldName) {
		return serialize(fieldName).getPattern();				
	}
	
	public abstract SerializerInformation<N> serialize(String fieldName);
	
	public abstract N deserialize(String fieldName, HashMap<String, Object> columns);
	
}
