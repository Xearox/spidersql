package de.syranda.spidermysql.exceptions;

public class QueryNotExecutedException extends RuntimeException {

	private static final long serialVersionUID = -7113025411633801940L;
	
	public QueryNotExecutedException(String message) {
		super(message);
	}
	
}
