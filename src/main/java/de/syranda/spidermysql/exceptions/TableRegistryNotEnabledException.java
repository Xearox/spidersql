package de.syranda.spidermysql.exceptions;

public class TableRegistryNotEnabledException extends RuntimeException {

	private static final long serialVersionUID = 2590445833861925666L;

	public TableRegistryNotEnabledException(String message) {
		super(message);
	}
	
}
