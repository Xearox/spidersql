package de.syranda.spidermysql.table;

import java.sql.ResultSet;
import java.util.function.Consumer;

import de.syranda.spidermysql.ConnectionManager;

public class ResultQuery {

	private String query;
	private Object[] values;
	
	private Runnable onFailure;
	private Runnable onSuccess;
	
	private Boolean success = false;
	
	public ResultQuery(String query) {
		this(query, new Object[0]);
	}
		
	public ResultQuery(String query, Object... values) {
		this.query = query;		
		this.values = values;
	}
	
	public String getQuery() {
		return query;
	}
	
	public RecordResult sync() {
		ResultSet rs = ConnectionManager.resultStatement(query, values);
		this.success = rs != null;
		
		if (this.success) {
		    if (onSuccess != null) {
			    onSuccess.run();
		    }
		} else {
		    if (onFailure != null) {
		    	onFailure.run();
		    }
		}   
		
		return new RecordResult(rs);
	}
	
	public void async(Consumer<RecordResult> consumer) {
		ConnectionManager.threadPool().execute(() -> consumer.accept(sync()));
	}
	
	public ResultQuery onFailure(Runnable onFailure) {
		this.onFailure = onFailure;
		return this;
	}
	
	public ResultQuery onSuccess(Runnable onSuccess) {
		this.onSuccess = onSuccess;
		return this;
	}
	
	public Boolean isSuccess() {
		return success;
	}

}
