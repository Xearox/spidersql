package de.syranda.spidermysql.table;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import de.syranda.spidermysql.ConnectionManager;
import de.syranda.spidermysql.ConventionFormatter;
import de.syranda.spidermysql.builder.ColumnType;
import de.syranda.spidermysql.builder.Exclude;
import de.syranda.spidermysql.builder.ExcludeField;
import de.syranda.spidermysql.builder.MySQLClass;
import de.syranda.spidermysql.serializer.ClassSerializer;
import de.syranda.spidermysql.utils.Serializer;

/**
 * Class for better {@link ResultSet} handling
 * 
 * @author Boris
 *
 */
public class RecordResult {

	private HashMap<String, Object[]> results = new HashMap<String, Object[]>();
	private HashMap<Class<?>, Object[]> resultsClasses = new HashMap<Class<?>, Object[]>();
	private int size = 0;
	private int index = -1;	
	
	/**
	 * Creates a new {@link RecordResult}
	 * 
	 * @param rs The {@link ResultSet} you want to handle
	 */
	public RecordResult(ResultSet rs) {
				
		try {
			rs.last();
			size = rs.getRow();
			rs.beforeFirst();
			int index = 0;
			
			resultSetCheck:
			while (rs.next()) {
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					
					String columnName = rs.getMetaData().getColumnName(i);
					
					if(columnName.equalsIgnoreCase("COLUMN_NAME")) break resultSetCheck;
					
					if (!results.containsKey(columnName)) {
						results.put(columnName, new Object[size]);
					}
					
					results.get(columnName)[index] = rs.getObject(columnName);
										
				}
				index++;
			}
				
			rs.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}	
				
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return Object[] The set of all values of the given column
	 */
	public Object[] getObject(String col) {
		return results.get(col);		
	}

	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link Object} The first value of the given column
	 */
	public Object getFirstObject(String col) {		
		return getObject(col)[0];		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return boolean[] The set of all values of the given column
	 */
	public boolean[] getBoolean(String col) {
			
		boolean[] res = new boolean[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			res[i] = Integer.parseInt(getObject(col)[i].toString()) == 0 ? false : true;			
		
		return res;
		
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link Boolean} The first value of the given column
	 */
	public boolean getFirstBoolean(String col) {		
		return getBoolean(col)[0];		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return String[] The set of all values of the given column
	 */
	public String[] getString(String col) {
				
		String[] res = new String[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				res[i] = getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString();
			else 
				res[i] = getObject(col)[i] != null ? getObject(col)[i].toString() : null;			
		
		return res;
		
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link String} The first value of the given column
	 */
	public String getFirstString(String col) {		
		return getString(col)[0];		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return int[] The set of all values of the given column
	 */
	public int[] getInt(String col) {
		
		int[] res = new int[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				res[i] = Integer.parseInt(getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
			else
				res[i] = Integer.parseInt(getObject(col)[i].toString());			
		
		return res;
		
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link Integer} The first value of the given column
	 */
	public int getFirstInt(String col) {		
		return getInt(col)[0];		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return double[] The set of all values of the given column
	 */
	public double[] getDouble(String col) {
		
		double[] res = new double[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				res[i] = Double.parseDouble(getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
			else 
				res[i] = Double.parseDouble(getObject(col)[i].toString());			
		
		return res;
		
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link Double} The first value of the given column
	 */
	public double getFirstDouble(String col) {		
		return getDouble(col)[0];		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return long[] The set of all values of the given column
	 */
	public long[] getLong(String col) {
		
		long[] res = new long[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				res[i] = Long.parseLong(getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
			else 
				res[i] = Long.parseLong(getObject(col)[i].toString());			
		
		return res;
		
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link Long} The first value of the given column
	 */
	public long getFirstLong(String col) {		
		return getLong(col)[0];		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return Object[] The set of all values of the given column
	 */
	public Object[] getBlob(String col) {		
		
		Object[] res = new Object[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			try {
				if(col.matches("[^>]+>[^>]+"))
					res[i] = Serializer.deserialize(getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
				else {
					if(getObject(col)[i] == null) {
						res[i] = null;
					} else {
						res[i] = Serializer.deserialize(getObject(col)[i].toString());
					}
				}
			} catch(Exception e) {
				ConnectionManager.handleException(e, false);
			}
		
		return res;
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link Object} The first value of the given column
	 */
	public Object getFirstBlob(String col) {		
		return getBlob(col)[0];		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return JSONObject[] The set of all values of the given column
	 */
	public JSONObject[] getJSONObject(String col) {
		
		JSONObject[] res = new JSONObject[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			try {
				res[i] = (JSONObject) new JSONParser().parse(getObject(col)[i].toString());	
			} catch(Exception e) {}
		
		return res;
		
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link JSONObject} The first value of the given column
	 */
	public JSONObject getFirstJSONObject(String col) {		
		return getJSONObject(col)[0];		
	}
	
	public int getSize() {		
		return size;		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param col The column you want to get values of
	 * @return Object[] The set of all values of the given column
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object[] getObjectFromType(Class<?> clazz) {
		
		if (resultsClasses.containsKey(clazz)) {
			return resultsClasses.get(clazz);
		}
		
		Object[] ret = new Object[getSize()];

		if (!clazz.isAnnotationPresent(MySQLClass.class)) {
			return ret;
		}
		
		boolean adjust = clazz.getAnnotation(MySQLClass.class).adjustFields();
		
		for (int i = 0; i < ret.length; i++) {
			
			try {
				Constructor<?> consttructor = clazz.getDeclaredConstructor();
				consttructor.setAccessible(true);
				ret[i] = consttructor.newInstance();
			} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		
			for (Field field : clazz.getDeclaredFields()) {
				
				if (field.isAnnotationPresent(Exclude.class) || Modifier.isStatic(field.getModifiers())) {
					continue;				
				}
				
				String fieldName = adjust ? ConventionFormatter.getMySQLName(field.getName()) : field.getName();
				
				if (ClassSerializer.hasSerializer(field.getType())) {
					
					ClassSerializer<?> serializer = ClassSerializer.getSerializer(field.getType());
					
					HashMap<String, Object> deserialize = new HashMap<String, Object>();
					
					HashMap<String, Class<?>> pattern = serializer.getPattern(fieldName);
					
					if (field.isAnnotationPresent(ExcludeField.class)) {
						for (String exField : field.getAnnotation(ExcludeField.class).fields()) {
							pattern.remove(exField);
						}
					}
					
					for (Entry<String, Class<?>> entry : pattern.entrySet()) {					
						deserialize.put(entry.getKey(), getObject(entry.getKey())[i]);					 
					}
					
					try {
						field.setAccessible(true);
						field.set(ret[i], serializer.deserialize(fieldName, deserialize));
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
					
				} else {
					try {
						field.setAccessible(true);
						if (this.getObject(fieldName)[i] == null) {
							continue;
						}
						if (field.getType().isEnum()) {				
							field.set(ret[i], getEnum((Class<Enum>) field.getType(), fieldName)[i]);			
						} else if (Serializable.class.isAssignableFrom(field.getType()) && ColumnType.getType(field.getType()) == ColumnType.NONE) {
							field.set(ret[i], getBlob(fieldName)[i]);
						} else if (field.getType().equals(boolean.class) || field.getType().equals(Boolean.class)) {
							field.set(ret[i], getBoolean(fieldName)[i]);
						} else if (getObject(fieldName)[i] != null){
							field.set(ret[i], getObject(fieldName)[i]);
						}
							
						
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}	
				}
			}
			
		}
		
		resultsClasses.put(clazz, ret);
		
		return ret;
		
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param col The column you want to get values of
	 * @return {@link Object} The first value of the given column
	 */
	@SuppressWarnings("unchecked")
	public <N> N getFirstObjectFromType(Class<N> clazz) {	
		return (N) getObjectFromType(clazz)[0];		
	}
	
	/**
	 * Gets values of a given column
	 * 
	 * @param enumType The enum the value should be casted to
	 * @param col The column you want to get values of
	 * @return Object[] The set of all values of the given column
	 */
	public <T extends Enum<T>> Object[] getEnum(Class<T> enumType, String col) {		
		
		Object[] ret = new Object[getSize()];
		
		for(int i = 0; i < getSize(); i++)
			if(col.matches("[^>]+>[^>]+"))
				ret[i] = Enum.valueOf(enumType, getJSONObject(col.split(">")[0])[i].get(col.split(">", 2)[1]).toString());
			else 
				ret[i] = Enum.valueOf(enumType, getObject(col)[i].toString());
				
		return ret;
		
	}
	
	/**
	 * Gets the first value of a column
	 * 
	 * @param enumType The enum the value should be casted to
	 * @param col The column you want to get values of
	 * @return {@link Enum} The first value of the given column
	 */
	@SuppressWarnings("unchecked")
	public <T extends Enum<T>> T getFirstEnum(Class<T> enumType, String col) {		
		return (T) getEnum(enumType, col)[0];		
	}
	
	/**
	 * Gets whether the {@link RecordResult} has a next index
	 * 
	 * @return {@link Boolean} Whether the {@link RecordResult} has a next index
	 */
	public boolean next() {		
		index++;
		if(index == getSize())
			return false;
		else
			return true;		
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return Object The current value
	 */
	public Object getCurrentObject(String col) {
		return getObject(col)[index];		
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return boolean The current value
	 */
	public boolean getCurrentBoolean(String col) {			
		return getBoolean(col)[index];
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return String The current value
	 */
	public String getCurrentString(String col) {			
		return getString(col)[index];		
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return int The current value
	 */
	public int getCurrentInt(String col) {		
		return getInt(col)[index];		
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return double The current value
	 */
	public double getCurrentDouble(String col) {		
		return getDouble(col)[index];		
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return long The current value
	 */
	public long getCurrentLong(String col) {
		return getLong(col)[index];		
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return Object The current value
	 */
	public Object getCurrentBlob(String col) {			
		return getBlob(col)[index];		
	}		
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return Object The current value
	 */
	@SuppressWarnings("unchecked")
	public <N> N getCurrentObjectFromType(Class<N> clazz) {		
		return (N) getObjectFromType(clazz)[index];
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param col The column you want to get values of
	 * @return JSONObject The current value
	 */
	public JSONObject getCurrentJSONObject(String col) {		
		return getJSONObject(col)[index];		
	}
	
	/**
	 * Gets the value of the given column on the current index
	 * 
	 * @param enumType The enum the value should be casted to
	 * @param col The column you want to get values of
	 * @return Enum The current value
	 */
	@SuppressWarnings("unchecked")
	public <T extends Enum<T>> T getCurrentEnum(Class<T> enumType, String col) {		
		return (T) getEnum(enumType, col)[index];		
	}

	/**
	 * Returns the current index of the iteration
	 * 
	 * @return {@link Integer} The current index
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * Resets the index of this RecordResult
	 */
	public void resetIndex(){
		index = -1;
	}
	
	/**
	 * Returns a set with all column names
	 * 
	 * @return A set with all column names
	 */
	public Set<String> getColumnNames() {
		return results.keySet();
	}
	
}
