package de.syranda.spidermysql.table;

import java.util.stream.Stream;

import de.syranda.spidermysql.builder.ForeignKey;
import de.syranda.spidermysql.builder.Key;
import de.syranda.spidermysql.builder.TableBuilder;
import de.syranda.spidermysql.syntax.MySQLTableSyntax;
import de.syranda.spidermysql.utils.ConfigValues;

public class MySQLTable {
	
	private String tableName;
	private String database;
	
	public MySQLTable(String tableName) {		
		this(tableName, null);
	}
	
	public MySQLTable(String tableName, String database) {
		this.tableName = tableName;
		this.database = database;	
	}
	
	public String getTableName() {		
		return tableName;
	}
	
	public InsertQuery insert(String values) {		
		return new InsertQuery(new MySQLTableSyntax(this).insert(values), MySQLTableSyntax.getEscapedValues(values));			
	}

	public InsertQuery insertUpdate(String values) {		
		return new InsertQuery(new MySQLTableSyntax(this).insertUpdate(values), Stream.of(MySQLTableSyntax.getEscapedValues(values), MySQLTableSyntax.getEscapedValues(values)).flatMap(Stream::of).toArray());			
	}
	
	public InsertQuery insertIgnore(String values) {		
		return new InsertQuery(new MySQLTableSyntax(this).insertIgnore(values), MySQLTableSyntax.getEscapedValues(values));			
	}
	
	public InsertQuery insert(Object clazz, String add) {		
		return new InsertQuery(new MySQLTableSyntax(this).insert(clazz, add), MySQLTableSyntax.getEscapedValues(MySQLTableSyntax.getSpiderList(clazz, add)));			
	}

	public InsertQuery insert(Object clazz) { 	
		return new InsertQuery(new MySQLTableSyntax(this).insert(clazz), MySQLTableSyntax.getEscapedValues(MySQLTableSyntax.getSpiderList(clazz, "")));		
	}
	
	public InsertQuery insertUpdate(Object clazz, String add) {		
		return new InsertQuery(new MySQLTableSyntax(this).insertUpdate(clazz, add), Stream.of(MySQLTableSyntax.getEscapedValues(MySQLTableSyntax.getSpiderList(clazz, "")), MySQLTableSyntax.getEscapedValues(MySQLTableSyntax.getSpiderList(clazz, ""))).flatMap(Stream::of).toArray());			
	}
	
	public InsertQuery insertUpdate(Object clazz) {
		return new InsertQuery(new MySQLTableSyntax(this).insertUpdate(clazz), Stream.of(MySQLTableSyntax.getEscapedValues(MySQLTableSyntax.getSpiderList(clazz, "")), MySQLTableSyntax.getEscapedValues(MySQLTableSyntax.getSpiderList(clazz, ""))).flatMap(Stream::of).toArray());	
	}
	
	public InsertQuery insertIgnore(Object clazz, String add) {		
		return new InsertQuery(new MySQLTableSyntax(this).insertIgnore(clazz, add), MySQLTableSyntax.getEscapedValues(MySQLTableSyntax.getSpiderList(clazz, add)));		
	}
	
	public InsertQuery insertIgnore(Object clazz) {
		return new InsertQuery(new MySQLTableSyntax(this).insertIgnore(clazz), MySQLTableSyntax.getEscapedValues(MySQLTableSyntax.getSpiderList(clazz, "")));			
	}	
	
	public ResultQuery get(String where) {		
		return new ResultQuery(new MySQLTableSyntax(this).get(where), MySQLTableSyntax.getEscapedValues(where));		
	}
	
	public InsertQuery update(String what, String where) {		
		return new InsertQuery(new MySQLTableSyntax(this).update(what, where), Stream.of(MySQLTableSyntax.getEscapedValues(what), MySQLTableSyntax.getEscapedValues(where)).flatMap(Stream::of).toArray());		
	}
	
	public InsertQuery delete(String where) {		
		return new InsertQuery(new MySQLTableSyntax(this).delete(where), MySQLTableSyntax.getEscapedValues(where));		
	}
	
	public ResultQuery get() {		
		return new ResultQuery(new MySQLTableSyntax(this).get());		
	}
	
	public InsertQuery truncate() {		
		return new InsertQuery(new MySQLTableSyntax(this).truncate());		
	}
	
	public TableBuilder builder() {		
		return new TableBuilder(getTableName(), getDatabase());		
	}	
	
	public String getTarget() {
		return (database != null ? "`" + database + "`." : "") + "`" + tableName + "`";		
	}
	
	public String getDatabase() {
		return this.database;		
	}
	
	public ResultQuery showColumn(String field) {
		return new ResultQuery(new MySQLTableSyntax(this).showColumn(field)); 
	}
	
	public InsertQuery changeColumn(String field, MySQLField newField) {		
		return new InsertQuery(new MySQLTableSyntax(this).changeColumn(field, newField));	}
	
	public InsertQuery setColumnDefault(String field, Object newDefault) {		
		return new InsertQuery(new MySQLTableSyntax(this).setColumnDefault(field, newDefault));		
	}
	
	public InsertQuery changeColumnType(MySQLField field) {
		return new InsertQuery(new MySQLTableSyntax(this).changeColumnType(field));		
	}
	
	public InsertQuery addField(MySQLField field) {		
		return new InsertQuery(new MySQLTableSyntax(this).addField(field));
	}

	public InsertQuery dropColumn(String field) {		
		return new InsertQuery(new MySQLTableSyntax(this).dropColumn(field));		
	}

	public InsertQuery dropPrimaryKey() {		
		return new InsertQuery(new MySQLTableSyntax(this).dropPrimaryKey());		
	}
	
	public InsertQuery dropKey(String key) {		
		return new InsertQuery(new MySQLTableSyntax(this).dropKey(key));		
	}
	
	public InsertQuery dropForeignKey(String foreignKey) {		
		return new InsertQuery(new MySQLTableSyntax(this).dropForeignKey(foreignKey));		
	}
	
	public InsertQuery addKey(Key key) {		
		return new InsertQuery(new MySQLTableSyntax(this).addKey(key));		
	}
	
	public InsertQuery addForeignKey(ForeignKey key) {		
		return new InsertQuery(new MySQLTableSyntax(this).addForeignKey(key));		
	}
	
	public InsertQuery drop() {		
		return new InsertQuery(new MySQLTableSyntax(this).drop());		
	}
	
	public ResultQuery describe() {		
		return new ResultQuery(new MySQLTableSyntax(this).describe());		
	}
	
	public ResultQuery showKeys() {		
		return new ResultQuery(new MySQLTableSyntax(this).showKeys());		
	}
	public boolean exists() {
		RecordResult query = new ResultQuery(new MySQLTableSyntax(this).exists(), getTableName(), getDatabase() != null ? getDatabase() : ConfigValues.MYSQL_DATABASE).sync();
		return query.getInt("count")[0] == 1;		
	}
}
