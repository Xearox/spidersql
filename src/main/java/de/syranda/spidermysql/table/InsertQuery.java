package de.syranda.spidermysql.table;

import de.syranda.spidermysql.ConnectionManager;

public class InsertQuery {

	private String query;
	private Object[] values;
	private boolean executed = false;
	private Runnable onFailure;
	private Runnable onSuccess;
	
	private Boolean success;
	
	public InsertQuery(String query, Object... values) {
		this.query = query;
		this.values = values;
	}
	
	public InsertQuery(String query) {
		this(query, new Object[0]);
	}
	
	@Deprecated
	public InsertQuery(String query, boolean writeToBackup) {
		this(query);
	}
	
	public String getQuery() {
		return query;
	}
	
	public void sync() {
		executed = true;
		
		this.success = ConnectionManager.insertStatement(query, values);
		
		if (this.success) {
		    if (onSuccess != null) {
		    	onSuccess.run();
		    }
		} else {
		    if (onFailure != null){
		    	onFailure.run();
		    }
		}
	}
	
	public void async() {
		executed = true;
		ConnectionManager.threadPool().execute(() -> sync());
	}
	
	public InsertQuery onFailure(Runnable onFailure) {
		this.onFailure = onFailure;
		return this;
	}
	
	public InsertQuery onSuccess(Runnable onSuccess) {
		this.onSuccess = onSuccess;
		return this;
	}
	
	public Boolean isSuccess() {
		return success;
	}
	
	public boolean isExecuted() {
		return executed;
	}
	
}
