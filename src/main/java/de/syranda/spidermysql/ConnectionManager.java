package de.syranda.spidermysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import de.syranda.spidermysql.utils.ConfigValues;

/**
 * 
 * Singleton
 * Static methods to manage the communication between the MySQL server and the API
 * 
 * @author Boris
 *
 */
public final class ConnectionManager {

	// Singleton
	private ConnectionManager() {
	}
	
	private static HikariDataSource mainConnection;
	
	public static HikariDataSource getMainConnection() { return ConnectionManager.mainConnection; };
	
	private static List<HikariDataSource> dataSources = new ArrayList<>();
	private static SpiderExceptionHandler handler = new SpiderExceptionHandler() {		
		@Override
		public void handleException(Exception exception, boolean queryExecuted) {
			exception.printStackTrace();			
		}				
	};	
	
	/**
	 * Handles thrown exceptions
	 * Only used internally
	 * 
	 * @param exception The thrown exception
	 * @param queryExecuted Whether the query was executed by the server or not
	 */
	public static void handleException(Exception exception, boolean queryExecuted) {		
		handler.handleException(exception, queryExecuted);
	}
	
	/**
	 * Sets the exception handler which handles all exception submitted by the method {@link #handleException(Exception, boolean)}
	 * 
	 * @param handler The new exception handler
	 */
	public static void setExceptionHandler(SpiderExceptionHandler handler) {		
		ConnectionManager.handler = handler;		
	}
	
	/**
	 * Establishes a connection for the API
	 * 
	 * The default maximum pool size is 20
	 * The default idle pool size is 1
	 * Connection timeout is set to 5s
	 *  
	 * @param host The host
	 * @param port The port
	 * @param database The default database
	 * @param user The user
	 * @param password The user's password
	 * 
	 * @return Whether the connection was established
	 */
	public static void addConnection(String host, int port, String database, String user, String password) {		
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl("jdbc:mysql://" + host + ":" + port + "/" + database + "?useSSL=false");
		config.setUsername(user);
		config.setPassword(password);
		config.setMaximumPoolSize(500);
		config.setMinimumIdle(1);
		config.setConnectionTimeout(10000);
		config.addHealthCheckProperty("initializationFailFast ", "true");
		HikariDataSource source = new HikariDataSource(config);
		if(mainConnection == null) {
			mainConnection = source;
			ConfigValues.MYSQL_DATABASE = database;
		} else {
			dataSources.add(source);
		}
	}
	
	/**
	 * Executes a query.
	 * Depending on the configuration the query is logged.
	 * All {@link PreparedStatement}s are closed automatically
	 * 
	 * @param query The query to execute
	 * @param writeToBackUp Whether the query should be executed on the backup connections
	 */
	public static boolean insertStatement(String query, Object... values) {		
		
		if (ConfigValues.LOG_QUERY) {
			System.out.println(query);
		}
		
		PreparedStatement ps = null;
		
		try {
			Connection conn = mainConnection.getConnection();
			ps = conn.prepareStatement(query);
			for (int i = 0; i < values.length; i++) {
				ps.setObject(i + 1, values[i].equals("null") ? null : values[i]);
			}
			ps.executeUpdate();
			ps.close();		
			conn.close();
				
			for (HikariDataSource source : dataSources) {		
				Connection backup = source.getConnection();
				ps = backup.prepareStatement(query);
				for (int i = 0; i < values.length; i++) {
					ps.setObject(i + 1, values[i].equals("null") ? null : values[i]);
				}
				ps.executeUpdate();
				ps.close();
				backup.close();
			}
						
		} catch (SQLException e) {	
			handleException(e, true);
			return false;
		} finally {		
			try {
				if (ps != null && !ps.isClosed()) {
					ps.close();
				}
			} catch (SQLException e) {
				handleException(e, true);
			}			
		}
		return true;
	}
	
	/**
	 * Executes a query and returns a {@link ResultSet}.
	 * Depending on the configuration the query is logged.
	 *    
	 * @param query The query to execute
	 * @param values Values for {@link PreparedStatement}
	 * 
	 * @return {@link ResultSet} The {@link ResultSet} of the executed query
	 */
	public static ResultSet resultStatement(String query, Object... values) {
		if (ConfigValues.LOG_QUERY) {
			System.out.println(query);
		}
				
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			conn = mainConnection.getConnection();
			ps = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			for (int i = 0; i < values.length; i++) {
				ps.setObject(i + 1, values[i].equals("null") ? null : values[i]);
			}
			rs = ps.executeQuery();
			return rs;
			
		} catch (SQLException e) {
			handleException(e, true);
			return null;
		} finally {
			if (ps != null) {
				try {
					ps.closeOnCompletion();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			addToConnectionWatcherPool(conn, rs);
		}
		
	}

	/**
	 * Closes all connections
	 */
	public static void disconnect() {
		mainConnection.close();
		dataSources.forEach(source -> source.close());	
		threadPool().shutdown();
	}

	@Deprecated
	public static void insertStatement(String query, boolean b) {
		insertStatement(query);		
	}
	
	/**
	 * Prepares a statement
	 * 
	 * @param query The query you want to prepare
	 * @return {@link PreparedStatement} A prepared statement
	 */
	public static PreparedStatement prepareStatement(String query) {
		try {
			return mainConnection.getConnection().prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static ExecutorService service;
	
	/**
	 * @return {@link ExecutorService} A ThreadPool for async statements
	 */
	public static ExecutorService threadPool() {
		if (service == null) {
			service = Executors.newCachedThreadPool();
		}
		return service;
	}
	
	private static ExecutorService connectionWatcherPool;
	
	/**
	 * @param connection {@link Connection} The connection the thread should close
	 * @param rs {@link ResultSet} The result set the thread should wait on to be closed
	 */
	public static void addToConnectionWatcherPool(Connection connection, ResultSet rs) {
		if (connectionWatcherPool == null) {
			connectionWatcherPool = Executors.newCachedThreadPool(r -> {
				Thread thread = Executors.defaultThreadFactory().newThread(r);
				thread.setDaemon(true);
				return thread;
			});
		}
		connectionWatcherPool.execute(() -> {
			
			try {
				if (connection.isClosed() || rs.isClosed()) {
					return;
				}
				
				while (!rs.isClosed()) {
					Thread.sleep(10);
				}
				
				connection.close();
				
			} catch (SQLException | InterruptedException e) {
			} finally {
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e) {
					}
				}
			}
			
		});
	}
	
}
