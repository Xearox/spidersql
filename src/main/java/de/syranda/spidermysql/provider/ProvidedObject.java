package de.syranda.spidermysql.provider;

public class ProvidedObject<N> {

	private N object;
	private long lastAccess = System.currentTimeMillis();
	
	public ProvidedObject(N object) {
		this.object = object;
	}
	
	protected N getSilent() {
		return object;
	}
	
	public N get() {
		this.lastAccess = System.currentTimeMillis();
		return this.object;
	}
	
	public long getLastAccess() {
		return lastAccess;
	}
	
}
