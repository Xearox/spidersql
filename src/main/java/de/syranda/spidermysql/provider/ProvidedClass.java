package de.syranda.spidermysql.provider;

public interface ProvidedClass {

	public default MySQLObjectProvider<?> provider() {
		return MySQLObjectProvider.forClass(getClass());
	}
	
	public default void save() {
		provider().saveUnsafe(this);
	}
	
}
