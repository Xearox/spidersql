package de.syranda.spidermysql.provider;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import de.syranda.spidermysql.builder.TableBuilder;
import de.syranda.spidermysql.helper.IdField;
import de.syranda.spidermysql.provider.thread.ProviderThread;
import de.syranda.spidermysql.table.MySQLTable;

public class MySQLObjectProvider<N> {
	
	private ProviderThread thread = new ProviderThread();
		
	public static <M> MySQLObjectProvider<M> loadForClass(Class<M> clazz, Consumer<MySQLObjectProvider<M>> setup) {
		
		if (forClass(clazz) != null) {
			return forClass(clazz);
		}
		
		MySQLObjectProvider<M> provider = new MySQLObjectProvider<M>(clazz);
		
		if (setup != null) {
			setup.accept(provider);
			if (provider.getTable() == null) {
				provider.setup();
			}
		}
		
		providers.add(provider);
		
		return provider;
		
	}
	
	public static <M> MySQLObjectProvider<M> loadForClass(Class<M> clazz) {
		return loadForClass(clazz, null);
	}
	
	private static List<MySQLObjectProvider<?>> providers = new ArrayList<>();
	
	public static List<MySQLObjectProvider<?>> all() {
		return providers;
	}
	
	@SuppressWarnings("unchecked")
	public static <M> MySQLObjectProvider<M> forClass(Class<M> clazz) {
		return (MySQLObjectProvider<M>) providers.stream().filter(provider -> provider.getClazz() == clazz).findAny().orElse(null);
	}
	
	
	private Class<N> clazz;
	private List<ProvidedObject<N>> objects = new ArrayList<>();
	
	private boolean persistent = false;

	private boolean hasIdField = true;

	private int autoSave = 600;
	private int cachePersistence = 60;
	private long lastSave = System.currentTimeMillis();
	
	private String[] identifierFields;
	
	private MySQLTable table;
	
	public MySQLObjectProvider(Class<N> clazz) {
		
		if (!thread.isAlive()) {
			thread.start();
		}
		
		this.clazz = clazz;
	}
	
	public final String[] getIdentifierFields() {
		return identifierFields;
	}
	
	public Class<N> getClazz() {
		return clazz;
	}
	
	public final void setIdentifierFields(String... identifierFields) {
		this.identifierFields = identifierFields;
	}
	
	public final boolean isPersistent() {
		return persistent;
	}
	
	public final void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}
	
	public final int getAutoSave() {
		return autoSave;
	}
	
	public final void setAutoSave(int autoSave) {
		this.autoSave = autoSave;
	}
	
	public boolean hasIdField() {
		return hasIdField;
	}
	
	public final void setHasIdField(boolean hasIdField) {
		this.hasIdField = hasIdField;
	}
	
	public final long getLastSave() {
		return lastSave;
	}
	
	public final void setLastSave(long lastSave) {
		this.lastSave = lastSave;
	}
	
	public final void setup() {
		this.setup(clazz.getSimpleName() + "s", false);
	}
	
	public final void setup(boolean forcePatch) {
		this.setup(clazz.getSimpleName() + "s", forcePatch);
	}
	
	public final void setup(String tablename, boolean forcePatch) {
		
		Objects.requireNonNull(this.identifierFields, "The identifier function must be defined");
						
		this.table = new MySQLTable(tablename);
		
		if (forcePatch && this.table.exists()) {
			
			this.table.get().async(rr -> {
				
				this.table.drop().sync();
				this.build(table);
				
				while (rr.next()) {
					
					String insert = rr.getColumnNames().stream()
										.filter(column -> rr.getCurrentObject(column) != null && !rr.getCurrentObject(column).equals(""))
										.map(column -> column + ":" + rr.getCurrentObject(column))
										.collect(Collectors.joining(";"));
					
					table.insert(insert).async();
					
				}
				
			});
			
		} else if (forcePatch) {
			this.table.drop().sync();
			this.build(table);
		} else {
			this.build(table);
		}
		
		if (this.isPersistent()) {
			
			this.table.get().async(rr -> {
				while (rr.next()) {
					this.objects.add(new ProvidedObject<N>(rr.getCurrentObjectFromType(this.clazz)));			
				}
			});
			
		}
		
	}	
	
	private void build(MySQLTable table) {
		TableBuilder builder = table.builder();
		if (this.hasIdField()) {
			builder.addField(new IdField());
		}
		builder
			.addFieldsOfClass(this.clazz)
		.build();	
	}
	
	public final ObjectPromise<N> get(Object... identifier) {
				
		Objects.requireNonNull(this.table, "The provider has not been setted up properly");
		
		ObjectPromise<N> promise = new ObjectPromise<N>();
		
		if (this.identifierFields.length != identifier.length) {
			throw new RuntimeException("Uneven counts of identfiers and idenfier fields");
		}
		
		ProvidedObject<N> ret = this.objects.stream().filter(obj -> {
			
			for (int i = 0; i < identifier.length; i++) {
				if (this.getField(this.identifierFields[i], obj.getSilent()) != identifier[i]) {
					return false;
				}
			}
			
			return true;
			
		}).findAny().orElse(null);
		
		if (ret != null) {
			promise.provide(ret.get());
		} else {

			StringBuilder builder = new StringBuilder();
			
			for (int i = 0; i < identifier.length; i++) {
				builder.append(this.identifierFields[i]);
				builder.append(":");
				builder.append(identifier[i]);
			}
			
			this.table.get(builder.toString()).async(rr -> {				
				if (rr.next()) {
					ProvidedObject<N> obj = new ProvidedObject<N>(rr.getCurrentObjectFromType(this.clazz));
					this.objects.add(obj);
					promise.provide(obj.get());
				} else {
					promise.cancel();
				}
			});
			
		}
		
		return promise;
		
	}
	
	public final MySQLTable getTable() {
		return table;
	}
	
	private final Object getField(String fieldName, Object obj) {
		try {
			
			Field field = obj.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.get(obj);
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public final void save(N object) {
		if (this.objects.stream().noneMatch(pObject -> pObject.getSilent().equals(object))) {
			this.objects.add(new ProvidedObject<N>(object));
		}
		this.table.insertUpdate(object).async();
	}
	
	public final List<ProvidedObject<N>> cache() {
		return this.objects;
	}
	
	@SuppressWarnings("unchecked")
	public final void saveUnsafe(Object object) {
		this.save((N) object);
	}
	
	public final int getCachePersistence() {
		return cachePersistence;
	}
	
	public final void setCachePersistence(int cachePersistence) {
		this.cachePersistence = cachePersistence;
	}
	
	public final void autoSave() {
		this.lastSave = System.currentTimeMillis();
		this.objects.forEach(pObject -> this.save(pObject.getSilent()));
	}
	
	public final void removeFromCache(List<ProvidedObject<?>> pObjects) {
		pObjects.forEach(pObject -> {
			this.saveUnsafe(pObject.getSilent());
		});
		this.objects.removeAll(pObjects);
	}
	
}
