package de.syranda.spidermysql.provider.thread;

import java.util.stream.Collectors;

import de.syranda.spidermysql.provider.MySQLObjectProvider;

public class ProviderThread extends Thread {

	@Override
	public void run() {

		while (!isInterrupted()) {
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			MySQLObjectProvider.all().forEach(provider -> {
				
				if (System.currentTimeMillis() >= provider.getLastSave() + provider.getAutoSave() * 1000) {
					provider.autoSave();
				}			
				
				if (provider.isPersistent()) {
					return;
				}
				
				provider.removeFromCache(provider.cache().stream().filter(pObject -> System.currentTimeMillis() - pObject.getLastAccess() >= provider.getCachePersistence() * 1000).collect(Collectors.toList()));
				
			});
			
		}
		
	}
	
}
