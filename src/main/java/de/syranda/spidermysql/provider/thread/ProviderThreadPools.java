package de.syranda.spidermysql.provider.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ProviderThreadPools extends Thread {

	private ProviderThreadPools() {
	}
	
	private static ExecutorService asyncService;
	
	public static ExecutorService asyncService() {
		if (asyncService == null) {
			asyncService = Executors.newCachedThreadPool();
		}
		return asyncService;		
	}
	
}
