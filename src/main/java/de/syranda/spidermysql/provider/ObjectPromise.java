package de.syranda.spidermysql.provider;

import java.util.function.Consumer;

import de.syranda.spidermysql.provider.thread.ProviderThreadPools;

public class ObjectPromise<N> {

	private volatile N object;
	private volatile boolean cancelled = false;
	
	public void subscribe(Consumer<N> consumer) {
		ProviderThreadPools.asyncService().submit(() -> {
			
			while (ObjectPromise.this.object == null && !ObjectPromise.this.isCancelled()) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
			}
			
			if (!ObjectPromise.this.isCancelled()) {
				consumer.accept(object);
			}
			
		});
	}
	
	public void provide(N object) {
		this.object = object;
	}
	
	public boolean isCancelled() {
		return cancelled;
	}
	
	public void cancel() {
		this.cancelled = true;
	}
	
}
