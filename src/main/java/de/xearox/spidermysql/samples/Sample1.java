package de.xearox.spidermysql.samples;

import de.syranda.spidermysql.ConnectionManager;
import de.syranda.spidermysql.builder.ColumnType;
import de.syranda.spidermysql.helper.PrimaryKey;
import de.syranda.spidermysql.table.MySQLField;
import de.syranda.spidermysql.table.MySQLTable;
import de.syranda.spidermysql.table.RecordResult;

public class Sample1 {
	
	/*
	 * The hostname where the MySQL Server is located
	 */
	static String hostname = "localhost";
	
	/*
	 * The port on which the MySQL Server is listening
	 */
	static int port = 3306;
	
	/*
	 * The username to login to the MySQL Server
	 */
	static String username = "username";
	
	/*
	 * The password to login to the MySQL Server
	 */
	static String password = "password";
	
	/*
	 * The database which you want to use
	 */
	static String database = "sample_database";
	
	/*
	 * Optional: The table prefix for all tables
	 */
	static String tablePrefix = "smpl";
	
	/*
	 * A SpiderMySQLTable to get easier access for later usages  
	 */
	static MySQLTable personTable = new MySQLTable(tablePrefix + "person");
	
	
	
	public static void main(String[] args) {
		/*
		 * First we need to add a main connection to SpiderMySQL
		 * We are using the ConnectionManager to do it.
		 */
		ConnectionManager.addConnection(hostname, port, database, username, password);
		
		/*
		 * Now we can create the table.
		 * I've created a method, which will do that
		 */
		Sample1.createPersonTable();
		
		/*
		 * After that, we can put some data into the table
		 */
		Sample1.addNewPerson("Sven", "Trump", "19.01.1941");
		Sample1.addNewPerson("Josef", "Strauss", "25.04.1954");
		Sample1.addNewPerson("Hans Josef", "Schmitz", "04.12.1965");
		Sample1.addNewPerson("Ulrich", "Schmitz", "07.10.1965");
		Sample1.addNewPerson("Donald", "Eichenfeld", "19.01.1903");
		Sample1.addNewPerson("Sasha", "Schmitz", "19.01.1993");
		Sample1.addNewPerson("Eric", "Knabe", "04.12.1993");
		Sample1.addNewPerson("Leopold", "Walter", "25.03.1893");
		
		/*
		 * Now we need to get some data.
		 * In this case, we just print the data out
		 */
		Sample1.printAllPersons();
		
		/*
		 * This is the output
		 * 1 ### forename: Donald ### surname: Eichenfeld ### birthday: 19.01.1903
		 * 2 ### forename: Eric ### surname: Knabe ### birthday: 04.12.1993
		 * 3 ### forename: Hans Josef ### surname: Schmitz ### birthday: 04.12.1965
		 * 4 ### forename: Josef ### surname: Strauss ### birthday: 25.04.1954
		 * 5 ### forename: Leopold ### surname: Walter ### birthday: 25.03.1893
		 * 6 ### forename: Sasha ### surname: Schmitz ### birthday: 19.01.1993
		 * 7 ### forename: Sven ### surname: Trump ### birthday: 19.01.1941
		 * 8 ### forename: Ulrich ### surname: Schmitz ### birthday: 07.10.1965
		 */
		
		System.out.println("");
		System.out.println("");
		System.out.println("");
		
		/*
		 * If we want to get all persons with the same surname
		 * we call the next method.
		 */
		Sample1.printAllPersonsWithGivenSurname("Schmitz");
		
		/*
		 * This is the output
		 * 1 ### forename: Hans Josef ### surname: Schmitz ### birthday: 04.12.1965
		 * 2 ### forename: Sasha ### surname: Schmitz ### birthday: 19.01.1993
		 * 3 ### forename: Ulrich ### surname: Schmitz ### birthday: 07.10.1965
		 */
	}
	
	/*
	 * This method is used to create the person table
	 */
	static void createPersonTable(){
		/*
		 * First we need to get the SpiderMySQLTable which we are created on the top
		 * and then we can get access to the builder.
		 */
		Sample1.personTable.builder()
			/*
			 * Then we can add some columns. For this samle, I am using
			 * the columns: forename, surname, birthday.
			 * Also the columns day, month and year
			 * Also I will add AutoIncrement.
			 * To make sure, that we have only one person with the same name and birthday
			 * we are adding a Composite Primary Key
			 * 
			 * Take a look on this: http://weblogs.sqlteam.com/jeffs/archive/2007/08/23/composite_primary_keys.aspx
			 */
			.addField(new MySQLField("forename", ColumnType.STRING))
			.addField(new MySQLField("surname", ColumnType.STRING))
			.addField(new MySQLField("birthday", ColumnType.STRING))
			.addField(new MySQLField("day", ColumnType.INT))
			.addField(new MySQLField("month", ColumnType.INT))
			.addField(new MySQLField("year", ColumnType.INT))
			.addKey(new PrimaryKey("forename", "surname", "birthday"))
			/*
			 * To create this table, we use the build method.
			 * This table will only be created, if the table does NOT exists.
			 */
			.build();
	}
	/**
	 * 
	 * @param forename The forename of the person
	 * @param surname The surname of the person
	 * @param birthday The birthday of the person
	 * @return Return false, if the creation was not successful otherwise true 
	 */
	static boolean addNewPerson(String forename, String surname, String birthday){
		/*
		 * First, we need to create a query.
		 */
		String query = "";
		
		/*
		 * Second, we should check, if the input is filled and not null
		 * If it empty or null we will return false
		 */
		if(forename == null) return false; if(forename.equals("")) return false;
		if(surname == null) return false;  if(surname.equals("")) return false;
		if(birthday == null) return false; if(birthday.equals("")) return false;
		
		/*
		 * Now we can create the query
		 * Follow this syntax: columnName:columnValue;
		 * In SpiderMySQLVersion you need to check if the last record doesn't has a semicolon at the end
		 * Otherwise the SpiderMySQLTableSyntax will return an exception
		 */
		query += "forename:" + forename + ";";
		query += "surname:" + surname + ";";
		query += "birthday:" + birthday + ";";
		
		/*
		 * Now we want to fill the other three columns.
		 * First, we split the whole date into a array of strings
		 */
		String[] splittedBirthday = birthday.split("\\.");
		
		/*
		 * Second, we fill the columns.
		 */
		query += "day:" + splittedBirthday[0] + ";";
		query += "month:" + splittedBirthday[1] + ";";
		query += "year:" + splittedBirthday[2] + ";";
		
		/*
		 * After we have created the query, we can call the SpiderMySQL Table to insert this record
		 * and to run the insertUpdate we need to call sync or async method
		 */
		Sample1.personTable.insertUpdate(query).sync();
		
		/*
		 * Now we can return true
		 */
		return true;
		
	}
	
	static void printAllPersons(){
		/*
		 * First, we need to get the SpiderMySQL Table
		 */
		MySQLTable table = Sample1.personTable;
		
		/*
		 * Second, we need to get the SpiderMySQL RecordResult
		 * To get this, we need to call the get() method from the SpiderMySQLTable
		 * And then we call the sync() method to get the RecordResult
		 */
		RecordResult result = table.get().sync();
		
		/*
		 * We have now some option to itrate through the RecordResult.
		 * Here I will call the slowest option.
		 * Slowest, because with a lot of data it will be very slow.
		 */
		for(int index = 0; index < result.getSize(); index++){
			/*
			 * First, we create an empty String call output
			 */
			String output = "";
			
			/*
			 * Second, we call the getString(columnName) method from RecordResult
			 * We get an array of data.
			 */
			output += "" + (index + 1);
			output += " ### forename: " + result.getString("forename")[index];
			output += " ### surname: " + result.getString("surname")[index];
			output += " ### birthday: " + result.getString("birthday")[index];
			
			/*
			 * Now we can print it out
			 */
			System.out.println(output);
		}
	}
	
	static void printAllPersonsWithGivenSurname(String surname){
		/*
		 * First, we need to get the SpiderMySQL Table
		 */
		MySQLTable table = Sample1.personTable;
		
		/*
		 * Second, we need to get the SpiderMySQL RecordResult
		 * To get this, we need to call the get() method from the SpiderMySQLTable
		 * But this time, we call the get() method with a parameter.
		 * The parameter is column:value
		 * 
		 * And then we call the sync() method to get the RecordResult
		 * It will return an RecordResult with all records where the
		 * surname column cell is the same as the given surname's parameter.
		 */
		RecordResult result = table.get("surname:"+surname).sync();
		
		/*
		 * To get all the persons with the same surname, we iterate
		 * through the RecordResult
		 */
		for(int index = 0; index < result.getSize(); index++){
			/*
			 * First, we create an empty String call output
			 */
			String output = "";
			
			/*
			 * Second, we call the getString(columnName) method from RecordResult
			 * We get an array of data.
			 */
			output += "" + (index + 1);
			output += " ### forename: " + result.getString("forename")[index];
			output += " ### surname: " + result.getString("surname")[index];
			output += " ### birthday: " + result.getString("birthday")[index];
			
			/*
			 * Now we can print it out
			 */
			System.out.println(output);
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
